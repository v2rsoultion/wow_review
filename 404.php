<?php include('header.php'); ?>
<!-- 404  Section -->
<section id="thankyou">
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 text-center wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<div class="thankyou_text pagenotfound">Page Not Found</div>
				<p></p>
				<a href="index.php" title="Back To Home">Homepage</a>
			</div>
			<div class="col-md-2"></div>
		</div> 
	</div>
</section>

<?php include('footer.php'); ?>