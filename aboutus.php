<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
				<h1>About <span> Us</span></h1>
				<p>Home / <a href="" title="Write a review">About Us</a></p>
			</div>
		</div>
	</div>
</section>

<!-- About Body Section -->
<section class="about_body">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<h2>Sed ut perspiciatis unde omnis iste natus error 
					<span>sit voluptatem</span> accusa
					ntium.
				</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<img src="images/a1.jpg" alt="about us">
			</div>
		</div>
	</div>
</section>
<section class="about_body" id="about_background">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12 text-left wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<img src="images/a1.jpg" alt="about us">
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<h2>Sed ut perspiciatis unde omnis iste natus error 
					<span>sit voluptatem</span> accusa
					ntium.
				</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
			</div>
		</div>
	</div>
</section>

  <!-- Recent Blog Section -->
    <section id="recent_blog">
        <div class="container padding_remove">
            <div class="row">
                <div class="col-md-12 common_heading text-center">
                    <h1 class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">Recent <span>Blog</span></h1>
                    <img src="images/u.png" alt="border">
                </div>
              </div>
              <div class="row">
              	<div class="col-md-12">  
                <!--  Start owl-carousel -->
 		      <div class="owl-carousel owl-theme wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"" id="about_blog">
                <div class="item ">
                    <div class="blog_img">
                        <img src="images/w.jpg" alt="blog">
                            
                    </div>
                    <div class="blog_detail">
                        <a href="" title="Share">
                            <div class="sharing"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
                        </a>
                        <img src="images/date.png" alt="date"> <span>June 13, 20171</span>
                        <h4>Lorem ipsum dolor sit</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consec tetur adipiscing elit, sed do eiusmod tempor aliqua Ut enim ad minim veniam.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <div class="blog_img">
                        <img src="images/w.jpg" alt="blog">

                    </div>
                    <div class="blog_detail">
                        <a href="" title="Share">
                            <div class="sharing"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
                        </a>
                        <img src="images/date.png" alt="date"> <span>June 13, 2017</span>
                        <h4>Lorem ipsum dolor sit</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consec tetur adipiscing elit, sed do eiusmod tempor aliqua Ut enim ad minim veniam.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <div class="blog_img">
                        <img src="images/w.jpg" alt="blog">
                    </div>
                    <div class="blog_detail">
                        <a href="" title="Share">
                            <div class="sharing"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
                        </a>
                        <img src="images/date.png" alt="date"> <span>June 13, 2017</span>
                        <h4>Lorem ipsum dolor sit</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consec tetur adipiscing elit, sed do eiusmod tempor aliqua Ut enim ad minim veniam.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <div class="blog_img">
                        <img src="images/w.jpg" alt="blog">
                    </div>
                    <div class="blog_detail">
                        <a href="" title="Share">
                            <div class="sharing"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
                        </a>
                        <img src="images/date.png" alt="date"> <span>June 13, 2017</span>
                        <h4>Lorem ipsum dolor sit</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consec tetur adipiscing elit, sed do eiusmod tempor aliqua Ut enim ad minim veniam.
                        </p>
                    </div>
                </div>
                 <div class="item">
                    <div class="blog_img">
                        <img src="images/w.jpg" alt="blog">
                    </div>
                    <div class="blog_detail">
                        <a href="" title="Share">
                            <div class="sharing"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
                        </a>
                        <img src="images/date.png" alt="date"> <span>June 13, 2017</span>
                        <h4>Lorem ipsum dolor sit</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consec tetur adipiscing elit, sed do eiusmod tempor aliqua Ut enim ad minim veniam.
                        </p>
                    </div>
                </div>
                 <div class="item">
                    <div class="blog_img">
                        <img src="images/w.jpg" alt="blog">
                    </div>
                    <div class="blog_detail">
                        <a href="" title="Share">
                            <div class="sharing"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
                        </a>
                        <img src="images/date.png" alt="date"> <span>June 13, 2017</span>
                        <h4>Lorem ipsum dolor sit</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consec tetur adipiscing elit, sed do eiusmod tempor aliqua Ut enim ad minim veniam.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!--  End owl-carousel -->

	    <div class="clearfix"></div>
	    <div class="view_all wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10""><a href="" title="View all">View all</a></div>
   </div>
  </div>
</section>


<!-- Include Footer  -->
<?php include('footer.php'); ?>