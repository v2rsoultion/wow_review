<?php include('header.php'); ?>
    <!-- Common Section -->
    <section id="common_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
                    <h1>Change <span> Password</span></h1>
                    <p>Home / <a href="" title="Change Password">Change Password</a></p>
                </div>
            </div>
        </div>
    </section>
    <section class="edit_profile" id="common_password">
        <div class="container">
            <div class="row">
                <div class="col-md-12 my_account">
                        <div class="col-md-6 col-xs-12 padding_remove ">
                        	 <form class="" action="" method="" id="changepassword" autocomplete="off">
                            <div class="form-group">
                                <input type="password" name="currentpassword" placeholder="Enter Your Current Password" class="form-control" autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <input type="password" name="newpassword" placeholder="Enter Your New Password" class="form-control" autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <input type="password" name="retypepassword" placeholder="Enter Your Re-type Password" class="form-control" autocomplete="new-password">
                            </div>
                            <button type="submit" title="Change Password" name="changepassword">Change Password</button>
                            <div class="clearfix"></div>
                         </form>
                    </div>
                   
                    <div class="col-md-6 myAccount wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" id="changepaspro">
                        <h4>My Account</h4>
                       <br>
                        <ul>
                            <a href="my_profile.php" title="My Profile">
                                <li>My Profile</li>
                            </a>
                            <a href="my_reviews.php" title="My Reviews">
                                <li>My Reviews</li>
                            </a>
                            <a href="my_reviews.php" title="My Likes Reviews">
                                <li>My Likes Reviews</li>
                            </a>
                            <a href="#" title="My Point Table">
                                <li>My Point Table</li>
                            </a>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>
    <?php include('footer.php'); ?>