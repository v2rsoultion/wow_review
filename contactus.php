<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
				<h1>Contact <span> Us</span></h1>
				<p>Home / <a href="" title="Write a review">Contact Us</a></p>
			</div>
		</div>
	</div>
</section>

<!-- Map Section -->

<section id="map_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31509094.985500813!2d112.64469188858718!3d-35.84015154506876!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a392728c7de6209%3A0x98c23aca11c1376c!2sAlbany+Town+Hall!5e0!3m2!1sen!2sin!4v1518159133524" width="100%" height="480" frameborder="0" style="border:0" allowfullscreen></iframe>
				
			</div>
				<div class="col-md-4 col-sm-4 col-xs-12 contactus wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
					<h3 class="text-center"><img src="images/map.png" alt="map"></h3>
					<h4>Address
						<p>12345, Lorem ipsum dolor sit aet, adipiscing elit, sed do smod</p>
					</h4>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 contactus wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
					<h3 class="text-center"><img src="images/mail.png" alt="mail" class="mailclass"></h3>
					<h4>Email Us
						<p><a href="mailto:info@WOWreviews.com" title="mail-us">info@WOWreviews.com</a><br>
							<a href="mailto:info@WOWreviews.com" title="mail-us">info@WOWreviews.com</a>
						</p>
					</h4>

				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 contactus wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
					<h3 class="text-center"><img src="images/phone.png" alt="phone"></h3>
					<h4>Phone
						<p><a href="tel:1234567890" title="call-us">+91 123 456 7890</a><br>
							<a href="tel:1234567890" title="call-us">+91 123 456 7890</a>
						</p>
					</h4>
				</div>
				<!-- Contact Form -->
				  <form class="form-group" action=""  method="POST" id="form-contact" >

					<div class="col-md-6 inputBox wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" id="contactform">
						<div class="form-group">
			   			<input type="text" class="form-control" name="yourname" placeholder="Your Name"  required="required">
			   		</div>
			   			<div class="form-group">
			   			<input type="text" class="form-control" name="phoneno" placeholder="Phone Number"  required="required">
			   		</div>
			   			<div class="form-group">
			   			<input type="email" class="form-control" name="email" placeholder="Email Address"  required="required">
			   		</div>
					</div>
					<div class="col-md-6 inputBox wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" id="contactform">
							
						<textarea class="form-control" name="messages" placeholder="Message"></textarea>
					
					</div>
					<div class="clearfix"></div>
					<button  type="submit" name="save"  class="submit_now wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">Submit Now</button>

					</form>

			</div>
		</div>
	</div>
</section>



<!-- Include Footer  -->
<?php include('footer.php'); ?>