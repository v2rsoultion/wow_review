-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2020 at 03:59 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `v2rteste_wow_temp`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_wow`
--

CREATE TABLE `data_wow` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `departing` varchar(255) NOT NULL,
  `arrival` varchar(255) NOT NULL,
  `yourreview` varchar(255) NOT NULL,
  `airline` varchar(255) NOT NULL,
  `flight` varchar(255) NOT NULL,
  `journey1` varchar(255) NOT NULL,
  `seatnumber` varchar(255) NOT NULL,
  `aircraft_model` varchar(255) NOT NULL,
  `reviewsubject` varchar(255) NOT NULL,
  `punctuality_rating` varchar(255) NOT NULL,
  `service_rating` varchar(255) NOT NULL,
  `comfort_rating` varchar(255) NOT NULL,
  `staff_rating` varchar(255) NOT NULL,
  `beverages_rating` varchar(255) NOT NULL,
  `inflight_rating` varchar(255) NOT NULL,
  `money_rating` varchar(255) NOT NULL,
  `amenities_rating` varchar(255) NOT NULL,
  `overall_rating` varchar(255) NOT NULL,
  `class_rating` varchar(255) NOT NULL,
  `cleanliness_rating` varchar(255) NOT NULL,
  `children` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_wow`
--

INSERT INTO `data_wow` (`id`, `email`, `departing`, `arrival`, `yourreview`, `airline`, `flight`, `journey1`, `seatnumber`, `aircraft_model`, `reviewsubject`, `punctuality_rating`, `service_rating`, `comfort_rating`, `staff_rating`, `beverages_rating`, `inflight_rating`, `money_rating`, `amenities_rating`, `overall_rating`, `class_rating`, `cleanliness_rating`, `children`) VALUES
(45, 'pascal.schock@gmail.com', 'Varanasi', 'Mumbai', '', 'Indigo', '6E-579', '4 February 2018', '', '', '', '4', '3', '2', '3', '1', '1', '3', '1', '3', 'Economy', '4', 'no'),
(46, 'jpnclc@gmail.com', 'Sydney', 'Melbourne', '', 'Qantas', 'QF427', '25 January 2018', '20D', '', '', '3', '3', '3', '3', '2', '3', '2', '1', '3', 'Economy', '4', 'no'),
(47, 'stacey.hansen@escapetravel.com.au', 'Melbourne', 'Hong Kong', '', 'Virgin Australia', '89', '20/07/2017', '', '', '', '4', '5', '5', '5', '4', '4', '5', '5', '5', 'Business', '5', 'no'),
(48, 'Andrew@arkm.co.nz', 'AKL', 'MEL', '', 'Air NZ', 'NZ125', '6 February 2018', '', '', '', '5', '5', '4', '5', '3', '3', '4', '3', '4', 'Business', '4', 'no'),
(49, 'Andrew@arkm.co.nz', 'AKL', 'KIX', '', 'Air New Zealand', 'NZ ', '2 February 2018', '6A', '', '', '3', '5', '2', '5', '3', '4', '4', '5', '4', 'Business', '3', 'no'),
(50, 'Andrew@arkm.co.nz', 'KIX', 'AKL', '', 'Air New Zealand', 'NZ 98', '4 February 2018', '', '', '', '5', '3', '2', '5', '3', '4', '4', '5', '4', 'Business', '4', ''),
(51, 'Alexis.balkin@gmail.com', 'RIX', 'ARN', 'Flight was on time and staff was great. Love the free WIFI onboard.', 'Norwegian', 'DY4548', '28 December  2017', '5F', '', 'Norwegian Trip to Stocholm', '5', '5', '4', '4', '4', '5', '5', '4', '4', 'Economy', '4', 'no'),
(53, 'jtgnielsen@gmai.com', 'Singapore ', 'Shanghai Pudong ', '', 'Singapore Airlines', 'SQ828', '08/10/2017', '32A', '777-300', '', '3', '3', '4', '4', '3', '5', '3', '2', '4', 'Premium Economy', '4', 'no'),
(54, 'jtgnielsen@gmai.com', 'Shanghai Pudong ', 'Bangkok ', '', 'Thai ', 'TG665', '03/11/2017', '43K', 'A330-200', '', '3', '3', '3', '3', '3', '3', '3', '2', '3', 'Economy', '4', 'no'),
(55, 'jtgnielsen@gmai.com', 'Melbourne ', 'Singapore ', 'SIA is a class act in many ways, from staff to planes and their Premium Economy delivers a good balance of value and comfort. The only thing I never understand is why they always serve food either at midnight when most people just want to sleep as happens', 'Singapore Airlines ', 'SQ218', '8 February 2018', '34D', '777-300', 'Classic SIA.....it just works!', '4', '4', '4', '4', '3', '5', '3', '2', '4', 'Premium Economy', '4', 'no'),
(56, 'Andrew@arkm.co.nz', 'MEL', 'AKL', '', 'Air New Zealand', 'NZ 124', '7 February 2018', '7A', '777-200', '', '4', '5', '5', '5', '4', '3', '4', '5', '4', 'Business', '3', 'no'),
(57, 'Andrew@arkm.co.nz', 'AKL ', 'MEL', '', 'New Zealand', 'NZ 433', '8 February 2018', '3C ', 'A320', '', '5', '5', '4', '3', '1', '1', '5', '3', '3', 'Economy', '2', ''),
(58, 'Winemaker@badgersbrook.com.au', 'Melbourne', 'Chongqing', '', 'Tanjin Airlines', 'GS7946', '24/01/2018', '', '', '', '5', '5', '5', '4', '3', '2', '5', '5', '4', 'Business', '5', 'no'),
(59, 'Andrew@arkm.co.nz', 'Wellington', 'Auckland ', '', 'Air New Zealand', 'NZ 456', '8 February 2018', '3C', 'A320', '', '5', '4', '3', '5', '1', '1', '4', '3', '3', 'Economy', '2', 'no'),
(60, 'jtgnielsen@gmai.com', 'Singapore ', 'Mumbai', '', 'Singapore ', 'SQ422', '8 February 2018', '31H', 'A350-900', '', '4', '3', '4', '3', '3', '5', '3', '2', '4', 'Premium Economy', '4', 'no'),
(61, 'Andrew@arkm.co.nz', 'Auckland ', 'Sydney', '', 'Air New Zealand', 'NZ 108', '9 February 2018', '2A', 'A340', '', '4', '5', '4', '2', '2', '1', '1', '3', '2', 'Business', '4', 'no'),
(62, 'jtgnielsen@gmai.com', 'Bombay ', 'Jodhpur ', 'We all understand that delays happen but a 2 hour plus on a 1 hour and 15 minuets flight seems to be taking it a bit far! The flight overall was fine, great leg space and spacious seats was a plus! Unfortunately the travelers were well over the good sides', 'Air India', 'AI645', '9 February 2018', '16A', 'A321', 'Very late....', '1', '2', '3', '3', '3', '1', '3', '1', '3', 'Economy', '4', 'no'),
(63, 'Andrew@arkm.co.nz', 'Auckland ', 'Wellington ', '', 'Air New Zealand ', 'NZ 425', '12 February 2018', '3A', 'A320', '', '1', '5', '3', '5', '3', '1', '3', '3', '4', 'Economy', '5', 'no'),
(64, 'Andrew@arkm.co.nz', 'Wellington ', 'Auckland ', '', 'Air New Zealand ', 'NZ 446', '12 February 2018', '3A', 'A320', '', '1', '5', '3', '5', '3', '3', '4', '3', '4', 'Economy', '4', 'no'),
(65, 'jtgnielsen@gmai.com', 'Jodhpur ', 'Delhi', '', 'Air India', 'AI476', '12 February 2018', '7F', 'A320', '', '3', '2', '3', '2', '2', '1', '2', '1', '3', 'Economy', '3', 'no'),
(66, 'jtgnielsen@gmai.com', 'Delhi', 'Singapore ', '', 'Singapore ', 'SQ403', '12 February 2018', '35A', 'A380', '', '4', '4', '4', '4', '3', '4', '3', '3', '4', 'Premium Economy', '4', 'no'),
(67, 'jtgnielsen@gmai.com', 'Singapore ', 'Melbourne ', '', 'Singapore ', 'SQ207', '13 February 2018', '32D', 'A350-900', '', '4', '3', '4', '5', '3', '4', '3', '3', '4', 'Premium Economy', '3', 'no'),
(68, 'Alexis.balkin@gmail.com', 'BVA', 'ARN', 'Plane arrived 15 minutes early. \r\n', 'Ryanair ', 'FR9504', '12 February 2018', '10B', '', 'Ryanair Paris to Stockholm ', '5', '5', '4', '5', '4', '1', '5', '4', '4', 'Economy', '4', 'no'),
(69, 'Alexis.balkin@gmail.com', 'ARN', 'RIX', 'Plane arrived on time. Great to have WiFi onboard on this short 55 minute flight.', 'Norwegian ', 'DY4547', '15 February 2018', '7B', '737-800', 'Trip from Stockholm to Riga', '5', '5', '4', '5', '4', '5', '5', '4', '4', 'Economy', '4', 'no'),
(70, 'andrew@arkm.co.nz', 'Auckland', 'Wellington', '', 'Air New Zealand', 'NZ 429', '15 February 2018', '3C', 'A320', '', '3', '5', '4', '5', '3', '2', '3', '3', '4', 'Economy', '5', 'no'),
(71, 'Andrew@arkm.co.nz', 'Wellington ', 'Auckland', '', 'Air New Zealand', 'NZ 434', '16 February 2018', '8C', 'A320', '', '1', '5', '4', '5', '3', '4', '5', '3', '4', 'Economy', '3', 'no'),
(72, 'Andrew@arkm.co.nz', 'Auckland', 'Sydney', '', 'Air New Zealand', 'NZ 101', '19 February 2018', '2A ', 'A340', '', '5', '5', '4', '4', '1', '1', '2', '2', '2', 'Business', '4', 'no'),
(73, 'Andrew@arkm.co.nz', 'Sydney ', 'Auckland ', '', 'Air New Zealand ', 'NZ108', '19 February 2018', '2C', 'A340', '', '3', '5', '2', '4', '1', '1', '1', '2', '2', 'Business', '3', 'no'),
(74, 'Andrew@arkm.co.nz', 'Auckland', 'Nadi', '', 'Air New Zealand ', 'NZ 752', '20 February 2018', '3A', 'A320', '', '5', '5', '3', '5', '3', '4', '4', '4', '4', 'Economy', '5', 'no'),
(75, 'mark.lang@anz.com', 'Melbourne ', 'Sunshine Coast', '', 'Jetstar', 'JQ794', '16 February 2018', '7C', '', '', '5', '4', '2', '2', '1', '1', '4', '2', '3', 'Economy', '3', 'no'),
(76, 'lang.gang@bigpond.com', 'Melbourne ', 'Sunshine Coast', '', 'Jetstar', 'JQ794', '11 February 2018', '8A', '', '', '5', '3', '2', '2', '1', '1', '4', '2', '3', 'Economy', '3', 'no'),
(77, 'Andrew@arkm.co.nz', 'Auckland ', 'Sydney ', '', 'Air New Zealand', 'NZ 104', '22 February 2018', '7B', '777-300', '', '3', '5', '5', '5', '3', '4', '3', '4', '3', 'Business', '2', 'no'),
(78, 'Andrew@arkm.co.nz', 'Sydney ', 'Auckland ', '', 'Air New Zealand', 'NZ 110', '23 February 2018', '7A', '777-200', '', '4', '4', '5', '5', '3', '4', '2', '4', '3', 'Business', '2', 'no'),
(79, 'jtgnielsen@gmai.com', 'Sydney ', 'Melbourne ', '', 'Qantas ', 'QF439', '18 November 2017', '11F', '737-800', '', '3', '3', '3', '3', '2', '3', '3', '1', '3', 'Economy', '4', 'no'),
(80, 'Nhroncova@yahoo.com', 'PVg', 'SVO', 'Good value for money, fasted route to Budapest, best connection time and fairly punctual. The aircrafts are quite new, too. ', 'Aeroflot', 'SU207', '24 February 2018', '45F', '', '', '4', '4', '3', '3', '3', '4', '4', '3', '4', 'Economy', '4', 'no'),
(81, 'Nhroncova@yahoo.com', 'BUD', 'DWC', 'Budget airline, not comfortable seats but you expect that. Otherwise the staff is very friendly and fairly new aircrafts.', 'WizAit', 'W6 2497', '8 February 2018', '', '', '', '4', '4', '2', '4', '3', '3', '4', '3', '3', 'Economy', '4', 'no'),
(82, 'Nhroncova@yahoo.com', 'BOM', 'PVg', 'First time with Air India, pleasantly surprised. Very nice airport in Mumbai, new and very clean and organized. ', 'Air India', 'AI 348', '23 February 2018', '', '', '', '4', '4', '3', '4', '4', '4', '4', '4', '4', 'Economy', '4', 'no'),
(83, 'Andrew@arkm.co.nz', 'Auckland', 'Queenstown', '', 'Air New Zealand', 'NZ 617', '5 March 2018', '2A', 'A320', '', '5', '5', '3', '5', '1', '1', '2', '3', '3', 'Economy', '4', 'no'),
(84, 'Andrew@arkm.co.nz', 'Queenstown', 'Auckland', '', 'Air New Zealand', 'NZ634', '26th February 2018', '6C', 'A320', '', '5', '5', '3', '5', '1', '1', '3', '3', '3', 'Economy', '3', 'no'),
(85, 'Andrew@arkm.co.nz', 'Auckland', 'Nadi', '', 'Air New Zealand', 'NZ752', '1 March 2018', '3C', 'A320', '', '5', '5', '3', '5', '1', '4', '2', '3', '3', 'Economy', '4', 'no'),
(86, 'Andrew@arkm.co.nz', 'Nadi', 'Auckland', '', 'Air New Zealand', 'NZ53', '2 March 2018', '9A', '787-9', '', '5', '5', '3', '5', '4', '4', '3', '5', '4', 'Business', '4', 'no'),
(87, 'jtgnielsen@gmai.com', 'Melbourne ', 'Sydney ', '', 'Virgin ', 'VA841', '3 March 2018', '5B', '737-800', '', '5', '4', '4', '3', '3', '3', '3', '2', '4', 'Economy', '4', 'no'),
(88, 'Jpnclc@gmail.com', 'Melbourne ', 'Sydney ', '', 'Virgin', 'Va841', '3 March 2018', '5a', '737800', '', '5', '5', '3', '3', '3', '1', '3', '2', '4', 'Economy', '3', 'no'),
(89, 'Jtgnielsen@gmail.com', 'Sydney ', 'Melbourne ', '', 'Virgin ', 'VA888', '5 March 2018', '3F', '737-800', '', '3', '3', '4', '3', '3', '4', '3', '2', '3', 'Economy', '4', 'no'),
(90, 'Jpnclc@gmail.com', 'Sydney ', 'Melbourne ', '', 'Virgin ', 'VA888', '5 March 2018', '3E', '737-800', '', '3', '3', '3', '3', '2', '4', '3', '2', '3', 'Economy', '4', 'no'),
(91, 'Andrew@arkm.co.nz', 'Auckland', 'Nadi', '', 'Air New Zealand', 'NZ752', '6 March 2018', '2A', 'A320', '', '4', '5', '3', '3', '3', '4', '3', '4', '3', 'Economy', '4', 'no'),
(92, 'jtgnielsen@gmai.com', 'Melbourne ', 'Sydney ', '', 'Virgin ', '845', '30 March 2018', '5F', '737-800', '', '3', '3', '4', '3', '3', '4', '3', '2', '3', 'Economy', '4', 'no'),
(93, 'Jpnclc@gmail.com', 'Melbourne ', 'Sydney ', '', 'Virgin ', '5E', '30 March 2018', '', '', '', '3', '3', '4', '3', '3', '4', '3', '2', '3', 'Economy', '4', 'no'),
(94, 'jtgnielsen@gmai.com', 'Sydney ', 'Melbourne ', '', 'Virgin ', 'VA862', '2 April 2018', '5F', '737-800', '', '4', '3', '4', '4', '3', '4', '3', '2', '4', 'Economy', '4', 'no'),
(95, 'Jpnclc@gmail.com', 'Sydney ', 'Melbourne ', '', 'Virgin ', 'VA862', '2 April 2018', '5E', '737-800', '', '4', '3', '4', '3', '3', '4', '3', '2', '3', 'Economy', '4', 'no'),
(96, 'jtgnielsen@gmai.com', 'Melbourne ', 'Singapore ', '', 'Singapore ', 'SQ208', '15 April 2018', '31K', 'A350', '', '3', '3', '4', '3', '3', '4', '3', '3', '3', 'Premium Economy', '4', 'no'),
(97, 'jtgnielsen@gmai.com', 'Singapore ', 'Melbourne ', '', 'Singapore ', 'SQ237', '17 April 2018', '32K', 'B777-300', '', '3', '3', '4', '4', '3', '5', '3', '2', '4', 'Premium Economy', '4', 'no'),
(98, 'jtgnielsen@gmai.com', 'Melbourne ', 'Doha', 'Qatar Airways pride themselves in being a 5-Star Airline and after a this first trip I have to agree. Departing Melbourne you are offered the Qantas lounge and from there on service goes up a notch! The business class airbus seats were wonderfully comfort', 'Qatar Airways ', 'QR905', '22 April 2018', '16K', 'A380-800', 'Five Star Airline ', '4', '4', '5', '5', '5', '5', '4', '4', '5', 'Business', '5', 'no'),
(99, 'jtgnielsen@gmai.com', 'Doha', 'Copenhagen ', '', 'Qatar Airways ', 'QR163', '23 April 2018', '3K', '787-800', '', '4', '5', '5', '5', '4', '4', '3', '4', '5', 'Business', '4', 'no'),
(100, 'jtgnielsen@gmai.com', 'London Heathrow ', 'Manchester ', '', 'British Airways ', 'BA1386', '28 April 2018', '7A', 'A320', '', '4', '4', '3', '3', '1', '1', '3', '1', '4', 'Economy', '4', 'no'),
(101, 'jtgnielsen@gmai.com', 'Manchester ', 'London Heathrow ', 'Flying from Manchester to London on a 3rd BAflight over a few days I have begun to wonder what BA are trying to do? They position themselves as a premium airline but offer nothing complimentary on board! No not a single cup of coffee or tea! Clients can p', 'British Airways ', 'BA1403', '29 April 2018', '9A', 'A319', 'Is BA another low cost airline?', '5', '4', '3', '2', '2', '2', '3', '2', '3', 'Economy', '4', 'no'),
(102, 'jtgnielsen@gmai.com', 'London Heathrow ', 'Paris CDG', 'A short trip from London was made good by super friendly staff who offered a nice chocolate croissant and coffee to match for free!! Such a small detail but yet so simple that BA could learn from! I know who I shall choose next time! Thanks AF', 'Air France', 'AF1681', '30 April 2018', '6F', 'A319', 'Nice one Air France', '3', '3', '4', '4', '4', '3', '3', '3', '4', 'Economy', '4', 'no'),
(103, 'jtgnielsen@gmai.com', 'Paris Charles De Gaulle ', 'Doha', 'Even though we had a good hour delay the staff on board are excellent at taking care of their guests. The A380 business class seating is very nice and with great service and entertainment one does not quite like the feeling of the captains voice saying th', 'Qatar Airways ', 'QR42', '3 May 2018', '14A', 'A380-800', 'Another excellent 5-Star flight!', '2', '4', '4', '5', '5', '5', '3', '4', '5', 'Business', '5', 'no'),
(104, 'jtgnielsen@gmai.com', 'Doha', 'Melbourne ', 'Flying from the stunning airport of Doha you are given the opportunity to enjoy the excellent lounge, but itâ€™s not just the beautiful design but the excellent people that makes the experience so good. The experience continues as you fly off with super f', 'Qatar Airways ', 'QR904', '3 May 2018', '14K', 'A380-800', 'Great people ', '4', '4', '5', '5', '4', '5', '3', '4', '5', 'Business', '4', 'no'),
(105, 'jtgnielsen@gmai.com', 'Sydney ', 'Melbourne ', 'On a rainy afternoon it was excellent to see Qantas getting their planes off nice and early. The A330 makes a nice difference compared to the B737 with comfort and in-flight entertainment. Service was good and efficient ', 'Qantas ', 'QF449', '13 May 2018', '31k', 'A330-200', 'Steady QF flight', '4', '3', '3', '3', '3', '4', '3', '1', '4', 'Economy', '4', 'no');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_wow`
--
ALTER TABLE `data_wow`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_wow`
--
ALTER TABLE `data_wow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
