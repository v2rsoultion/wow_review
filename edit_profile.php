<?php include('header.php'); ?>
    <!-- Common Section -->
    <section id="common_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
                    <h1>Edit <span> Profile</span></h1>
                    <p>Home / <a href="" title="Change Password">Edit Profile</a></p>
                </div>
            </div>
        </div>
    </section>
    <section class="edit_profile" id="common_password">
        <div class="container">
            <div class="row">
                <div class="col-md-12 my_account">
                        <div class="col-md-6 col-xs-12 padding_remove ">
                        	 <form class="" action="" method="" id="editprofile" autocomplete="off">
                            <div class="form-group">
                                <input type="text" name="profilename" placeholder="Enter Your Name" class="form-control" autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <input type="text" name="phonenumber" placeholder="Enter Your Phone No." class="form-control" autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <input type="text" name="editemail" placeholder="Enter Your Email Address" class="form-control" autocomplete="new-password">
                            </div>

                              <div class="form-group">
                                <textarea name="edit_address" placeholder="Enter Your  Address" class="form-control" ></textarea>
                              
                            </div>
                            <button type="submit" title="Change Password" name="changepassword">Save Profile</button>
                            <div class="clearfix"></div>
                         </form>
                    </div>
                   
                    <div class="col-md-6 myAccount wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" id="changepaspro">
                        <h4>My Account</h4>
                       <br>
                        <ul>
                            <a href="my_profile.php" title="My Profile">
                                <li>My Profile</li>
                            </a>
                            <a href="#" title="My Reviews">
                                <li>My Reviews</li>
                            </a>
                              <a href="write_review1.php" title="My Likes Reviews">
                                <li>My Likes Reviews</li>
                            </a>
                            <a href="frequent_membership.php" title="Frequent Flyer Membership">
                                <li>Frequent Flyer Membership</li>
                            </a>

                               <a href="changepassword.php" title="Change Password">
                                <li>Change Password</li>
                            </a>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>
    <?php include('footer.php'); ?>