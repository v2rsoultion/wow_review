<!-- Start Footer Section -->
<footer>
    <section id="footer_main">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12 padding_remove wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <a href=" " title="WOW Reviews">
                        <img src="images/f_logo.png ">
                        </a>
                        <p class="">
                        Lorem ipsum dolor sit amet, consec sit tetr adi piscing elit, sed do eiusmod tempor aliqua Ut<br> enim consec adi.
                         </p>
                    
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 hidden-xs common_footer wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <h4>Quick Links</h4>
                    <ul>
                        <li><a href="aboutus.php" title="About Wow Reviews">About Wow Reviews</a></li>
                        <li><a href="blog.php" title="Our Blog ">Our Blog </a></li>
                        <li><a href="listing_page.php" title="">Review Listing</a></li>
                        <li><a href="contactus.php" title="Contact Us ">Contact Us</a></li>
                        <li><a href="my_profile.php" title="My Profile">My Profile</a></li>
                        <li><a href="faq.php" title="Frequently asked questions">Faq</a></li>
                        <li><a href="privacy_policy.php" title="Privacy policy">Privacy policy</a></li>
                        <li><a href="terms_condition.php" title="Terms and conditions">Terms and conditions</a></li>
                    </ul>
                </div>
               
                <div class="col-md-4 col-sm-6 col-xs-12 common_footer remo_padding1 wow fadeInRight " data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 ">
                    <h4>signup newsletter</h4>
                    <form class="form-horizontal" action="" id="newsletter">
                        <p>Lorem ipsum dolor sit amet, coseceur.</p>
                        <div class="form-group">
                            <div class="col-sm-8 col-xs-7 remo_padding1 ">
                                <input type="email " class="form-control " id="subscrition " placeholder="Enter your email " name="subscrition">

                            </div>
                            <div class="col-sm-4 col-xs-5 remo_padding1 ">
                                <button type="submit " class="btn btn-warning " title="Submit" alt="Submit">Submit</button>
                            </div>
                        </div>
                    </form>
                    <!-- Social Icons -->
                    <a href=" " title="facebook " class="facebookf "><i class="fab fa-facebook-f "></i></a>
                    <a href=" " title="twitter "><i class="fab fa-twitter "></i></a>
                    <a href=" " title=" " title="google "><i class="fab fa-google-plus-g "></i></a>
                    <a href=" " title="instagram "><i class="fab fa-instagram "></i></a>
                </div>
            </div>
        </div>
    </section>
    <section id="copyright">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 text-center wow fadeInUp" data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 ">
                    <p>Copyright &copy; 2018 WOW Reviews All Right Reserved.</p>
                </div>
            </div>
        </div>
    </section>
</footer>
<!-- End Footer Section -->
<!-- Script files -->
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="scripts/jquery.zoomslider.min.js"></script>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/bootstrap-slider.js"></script>
<script type="text/javascript" src="scripts/jquery.hislide.js" ></script>
<script src="scripts/bootstrap-formhelpers.min.js"></script>
<script src="scripts/owl.carousel.js"></script>
<script src="scripts/wow.js"></script>
<script src="scripts/bootstrapvalidator.min.js"></script>
<script src="scripts/bootstrap-select.min.js"></script>
<script type="text/javascript">
  window.onload = function () {
            var fileUpload = document.getElementById("fileupload");
            fileUpload.onchange = function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = document.getElementById("dvPreview");
                    dvPreview.innerHTML = "";
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    for (var i = 0; i < fileUpload.files.length; i++) {
                        var file = fileUpload.files[i];
                        if (regex.test(file.name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = document.createElement("IMG");
                                img.height = "100";
                                img.width = "100";
                                img.src = e.target.result;
                                dvPreview.appendChild(img);
                            }
                            reader.readAsDataURL(file);
                        } else {
                        
                            alert(file.name + " is not a valid image file.");
                            dvPreview.innerHTML = "";
                            return false;
                        }
                    }
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            }
        };

$(document).ready(function(){
  $(".wrapper").hide();
  $(".show_hide").show();
  
  $('.show_hide').click(function(){
  $(".wrapper").slideToggle();
  });

});

$(document).ready(function(){
  $("#testdiv").hide();
  $(".show_hide1").show();
  
  $('.show_hide1').click(function(){
  $("#testdiv").slideToggle();
  });

});

  $("#ex5").slider({

        precision: 2,
});
    $("#ex6").slider({

        precision: 2,
});
       $("#ex7").slider({

        precision: 2,
});
$("#ex8").slider({

        precision: 2,
});
$("#ex9").slider({

        precision: 2,
});
$("#ex10").slider({

        precision: 2,
});
    wow = new WOW({
        animateClass: 'animated',
        offset: 100,
        callback: function(box) {
            console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    });
    wow.init();
	$('#rotator-demo').hiSlide({
		interval: 3000,
		speed: 700
	});

    $('.form-js-label').find('input').on('input', function (e) {
  $(e.currentTarget).attr('data-empty', !e.currentTarget.value);
});
    // About Blog
var owl = $('#about_blog');
              owl.owlCarousel({
                loop: true,
                autoplay:true,
                margin:30,
                autoplay: true,
                autoplayTimeout: 1500,
               autoplayHoverPause:true,
  		        pagination : true,
  		        navigation : true,
  		       nav:true,
  		       dots:false,
  		       navText: [ '', '' ],
  		       navClass: [ 'prev1', 'next1' ],

                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  1000: {
                    items: 4
                  }
                }
              });
	var owl = $('#recentblog');
              owl.owlCarousel({
                loop: true,
                autoplay:true,
                margin:30,
                autoplay: true,
                autoplayTimeout: 10000,
               autoplayHoverPause:true,
  		        pagination : true,
  		        navigation : true,
  		       nav:false,
  		       dots:true,
  		       navText: [ '', '' ],
  		       navClass: [ 'prev1', 'next1' ],

                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  1000: {
                    items: 1
                  }
                }
              });

// Header Scrolling 

   $(window).bind('scroll', function() {

        if ($(window).scrollTop() > 200) {
            $(".menu").css("position", "fixed");
            $(".menu").css("top", "20px;");
            $(".menu_scroll").css("top", "-50px");
            $(".menu").css("padding-top", "20px");
            $(".wow_menu_scroll ul li").css("padding-bottom", "29px");
            $(".wow_menu_scroll ul li").css("padding-top", "64px");
            $('.login_process').addClass('login_process_scroll');
            $(".logo").css("padding-top", "38px");
            $(".logo").css("padding-bottom", "1px");
            $('.menu').addClass('menu_scroll');
            $('.logo').addClass('logo_scroll');
            $(".logo img").css("width", "80%");
            $('.wow_menu').addClass('wow_menu_scroll');
           
        } else if ($(window).scrollTop() < 200) {
            $(".menu").css("position", "static");
            $(".menu").css("top", "0px;");
            $(".wow_menu ul li").css("padding-bottom", "42px");
            $(".wow_menu ul li").css("padding-top", "42px");
            $(".menu_scroll").css("top", "0px");
            $(".logo_scroll").css("padding-top", "13px");
            $(".logo").css("padding-bottom", "11px");
            $(".menu").css("padding-top", "0px");
            $('.menu').removeClass('menu_scroll');
            $('.logo').removeClass('logo_scroll');
            $(".logo img").css("width", "89%");
            $('.wow_menu').removeClass('wow_menu_scroll');
            $('.login_process').removeClass('login_process_scroll');
            
        }

  });

   function openNav() {
  $("#mySidenav").css({"left": "0"});
}
function closeNav() {
  $("#mySidenav").css({"left": "-100%"});
}
        function showlogin(){
          document.getElementById('login').style.display='block';
          document.getElementById('register').style.display='none';
        document.getElementById('forgot_password').style.display='none';
       $("#mySidenav").css({"left": "-100%"});


        }
        function showlogin1(){
          document.getElementById('register').style.display='block';
          document.getElementById('login').style.display='none';
           document.getElementById('forgot_password').style.display='none';
             $("#mySidenav").css({"left": "-100%"});
        }

          function showforgot(){
          document.getElementById('forgot_password').style.display='block';
            document.getElementById('login').style.display='none';
             document.getElementById('register').style.display='none';
               $("#mySidenav").css({"left": "-100%"});
          
        }


        $(document).ready(function(){
          $(".change").on({
            click: function(){
              $(this).css("color", "#33bae7");
            }  

          });
        });


   function hide_review() {
  $(".collapseExample1").hide();
  $(".rating_submit").hide();
  
}
$(document).ready(function() {
  $('#form-register').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          firstname: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Name'
                  }
              }
          },
          lastname: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your last name'
                  }
              }
          },
            usermail: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Email'
                  },
                   emailAddress: {
                    message: 'Please Enter Valid Email'
                }
              }
          },
          userpassword: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Password'
                  }
              }
          },
        }
      }),
  $('#form_login').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       
      
            usermail: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Email'
                  },
                   emailAddress: {
                    message: 'Please Enter Valid Email'
                }
              }
          },
          userpassword: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Password'
                  }
              }
          }
        }
      }),




  $('#changepassword').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       
      
           currentpassword: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Current Password'
                  }
              }
          },
          newpassword: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter New  Your Password'
                  }
              }
          },
          retypepassword: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Re-Type Password'
                  }
              }
          }
        }
      }),


  $('#editprofile').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       
      
           profilename: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Name'
                  }
              }
          },
          phonenumber: {
              validators: {
                  
                   notEmpty: {
                        message: 'Please Enter your phone number'
                    },
                    phone: {
                        country: 'IN',
                        message: 'Please Entre vaild phone number'
                    }
              }
          },
          editemail: {
              validators: {
                  
                     notEmpty: {
                      message: 'Please Enter Your Email Address'
                  },
                  emailAddress: {
                    message: 'Please Enter Valid Email Address'
                }
              }
          },
           edit_address: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Address'
                  }
              }
          }
        }
      }),

   $('#newsletter').bootstrapValidator({

      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       
      
            subscrition: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Email'
                  },
                  emailAddress: {
                    message: 'Please Enter Valid Email'
                }
              }
          },
     
        }
      }),
   $('#forgot_password').bootstrapValidator({

      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       
      
            forgot_mail: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Registered Email'
                  },
                  emailAddress: {
                    message: 'Please Enter Valid Registered Email'
                }
              }
          },
     
        }
      }),


      $('#writereview').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       
      
            seat: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Seat No.'
                  }
              }
          },

             subject: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Subject'
                  }
              }
          },
           writereview: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Write Your Review'
                  }
              }
          },

           business: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Select Something'
                  }
              }
          },
           travellers: {
              validators: {
                  
                  notEmpty: {
                      message: 'Enter Any tips for fellow travellers'
                  }
              }
          },
     
        }
      }),

    $('#form-contact').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
             valid: 'uk-icon-check',
              invalid: 'uk-icon-times',
              validating: 'uk-icon-refresh'
      },
      fields: {
       
      
            yourname: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Name'
                  }
              }
          },
           phoneno: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter your phone number'
                    },
                    phone: {
                        country: 'IN',
                        message: 'Please Entre vaild phone number'
                    }
                }
            },
          email: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Your Email Id'
                  },
                   emailAddress: {
                    message: 'Please Enter Valid Email'
                }
              }
          }
             
        }
      }),
    $('#search_aero').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
      
            aeroname: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Select Airline Name or Code'
                  }
              }
          },
          aeroname1: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter flight number'
                  }
              }
          }
        }
      }),
     $('#resetpasswod').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
      
            resetnew: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter New Password'
                  }
              }
          },
          confirmpassword: {
              validators: {
                  
                  notEmpty: {
                      message: 'Please Enter Confirm  Password'
                  }
              }
          }
        }
      })
});

// Frequent Flyer Membership

$(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls form:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-add').addClass('btn-remove')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
    $(this).parents('.entry:first').remove();

    e.preventDefault();
    return false;
  });
});


// Faq Page

   
    $(document).on('show','.accordion', function (e) {
         //$('.accordion-heading i').toggleClass(' ');
         $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });
    
    $(document).on('hide','.accordion', function (e) {
        $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
        //$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
    });
</script>
</body>
</html>