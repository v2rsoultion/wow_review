<?php include('header.php'); ?>
    <!-- Common Section -->
    <section id="common_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
                    <h1>Edit <span> Profile</span></h1>
                    <p>Home / <a href="" title="Change Password">Edit Profile</a></p>
                </div>
            </div>
        </div>
    </section>
    <section class="edit_profile" id="common_password">
        <div class="container">
            <div class="row">
                <div class="col-md-12 my_account">
                    <div class="col-md-6 col-xs-12 padding_remove ">
                    	<h4>Frequent Flyer Membership</h4>
                    	 <br>
							            <div class="control-group" id="fields">
							            <div class="controls"> 
							                <form role="form" autocomplete="off">
							                    <div class="entry input-group" id="frequent_btn">
							                       <!--  <input class="form-control" name="fields[]" type="text" placeholder="Type something" /> -->
							                        <select class="form-control" name="fields[]" >
							                        	<option>My Airlines Membership</option>
							                        	<option>OneWorld</option>
							                        	<option>Skyteam</option>
							                        	<option>Star Alliance</option>
							                        	<option>Vanilla Alliance </option>
							                        	<option>U-Fly Alliance</option>
							                        	<option>U-Fly Alliance</option>
							                        	<option>Value Alliance</option>
							                        </select>
							                         <select class="form-control" name="fields[]" >
							                        	<option>Membership Type</option>
							                        	<option>Silver</option>
							                        	<option>Bronze</option>
							                        </select>
							                    		<span class="input-group-btn">
							                            <button class="btn-add" type="button">
							                                <span class="glyphicon glyphicon-plus"></span>
							                            </button>
							                        </span>
							                        <div class="clearfix"></div>
							                    </div>
							                      <div class="clearfix"></div>
							                      
							                </form>
							                <button type="submit" class="frequent_btn">Submit </button>
							         </div>
	       						</div>
                  </div>
                    <div class="col-md-6 col-xs-12 myAccount wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" id="changepaspro">
                        <h4>My Account</h4>
                       <br>
                        <ul>
                            <a href="my_profile.php" title="My Profile">
                                <li>My Profile</li>
                            </a>
                            <a href="#" title="My Reviews">
                                <li>My Reviews</li>
                            </a>
                              <a href="write_review1.php" title="My Likes Reviews">
                                <li>My Likes Reviews</li>
                            </a>

                               <a href="changepassword.php" title="Change Password">
                                <li>Change Password</li>
                            </a>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>
    <?php include('footer.php'); ?>