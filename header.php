<!DOCTYPE>
<html>
   <head>
      <title>WOW Airline Reviews | Your Voice In the Sky</title>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="stylesheet/wow_review.css"/>
      <link rel="stylesheet" type="text/css" href="stylesheet/responsive.css">
      <!-- <link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"> -->
   </head>
   <!-- Header Section  Fixed-->
   <body class="model_classess">
  <header>
    <section class="header-top menu">
        <div class="container">
            <div class="row">
                <!-- Site logo -->
                <div class="col-md-4 col-sm-4 col-xs-12 logo">
                  <span  onclick="openNav()"> <img src="images/menu1.png"></span>
                    <a href="index.php" title="">
                        <img src="images/logo.png" alt="WOW Airline Reviews" class="mainResposnive">
                    </a>
                </div>
                <span id="mySidenav" class="sidenav">
                  <a href="javascript:void(0)" class="closebtn" ><span>Hello Johen !</span><p onclick="closeNav()">&times;</p></a>
                <div class="col-md-5 wow_menu text-center padding_remove">
                    <nav class="">
                      <!-- Note : This Class use for hide some menu in desktop (class="desktop_hide") -->
                        <ul class="">
                           <li class="desktop_hide"><a href="index.php" title="Home"><i class="fas fa-home"></i>Home</a></li>
                            <li><a href="write_review.php" title="Write A Review"><i class="fas fa-edit"></i>Write A Review</a></li>
                            <li><a href="write_review1.php" title="Search Reviews"><i class="fas fa-search"></i>Search Reviews</a></li>
                            <li><a href="recent_review.php" title="Recent Reviews"><i class="far fa-clock"></i>Recent Reviews</a></li>
                            <li class="desktop_hide"><a href="aboutus.php" title="About Wow Reviews"><i class="fab fa-blogger"></i>About Wow Reviews</a></li>
                            <li class="desktop_hide"><a href="blog.php" title="Our Blog"><i class="fab fa-blogger"></i>Our Blog</a></li>
                            <li class="desktop_hide"><a href="my_profile.php" title="My Profiles"><i class="far fa-user"></i>My Profile</a></li>
                            <li class="desktop_hide"><a href="contactus.php" title="Contact Us"><i class="fas fa-cog"></i>Contact Us</a></li>
                            <li class="desktop_hide"><a href="faq.php" title="Frequently asked questions"><i class="fab fa-blogger"></i>Faq</a></li>
                            <li class="desktop_hide"><a href="privacy_policy.php" title="Privacy policy"><i class="fab fa-blogger"></i>Privacy policy</a></li>
                            <li class="desktop_hide"><a href="terms_condition.php" title="Terms and conditions"><i class="fab fa-blogger"></i>Terms and conditions</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-3 text-right padding_remove">
                    <ul class="login_process">
                        <li title="login" onclick="showlogin()"><i class="fas fa-sign-in-alt"></i>Log In</li>
                        <li title="Sign Up" onclick="showlogin1()"><i class="fas fa-sign-out-alt"></i>Sign Up</li>
                    </ul>
                </div>
                </span>
            </div>
        </div>
    </section>
</header>
  
      <!-- Sing Up Form -->
      <div id="register" class="modal">
         <form class="form-group form_inner animate " action=""  method="POST" id="form-register" >
            <div class="imgcontainer">
               <span onclick="document.getElementById('register').style.display='none'" class="close" title="Close">&times;</span>
            </div>
            <div class="form_margin">
               <div class="row">
                  <div class="col-md-6 col-xs-12">
                     <img src="images/l.png" alt="WOW Airline Reviews">
                  </div>
                  <div class="col-md-6 col-xs-12 padding_remove ">
                     <h5>Sign <span>Up</span></h5>
                     <div class="form-group">
                     <input type="text" name="firstname" id="" placeholder="First Name" class="form-control" required="required" >
                   </div>
                     <div class="form-group">
                     <input type="text" name="lastname" id="" placeholder="Last Name" class="form-control " required="required" >
                   </div>
                     <div class="form-group">
                     <input type="email" name="usermail" placeholder="Enter Your Email" class="form-control " required="required"  autocomplete="new-email">
                   </div>
                     <div class="form-group">
                     <input type="password" name="userpassword" placeholder="Enter Your Password" class="form-control " required="required"  autocomplete="new-password" >
                   </div>
                     <div class="col-md-6 col-xs-8 remember padding_remove  ">
                      <!--   <input type="checkbox" name=""><span>Remember My Details</span> -->

                     </div>
                     <div class="col-md-6 col-xs-4 padding_remove text-right">
                        <a href="#"   title="Sign In" class="singin" onclick="showlogin()">Sign in</a>
                     </div>
                     <div class="clearfix"></div>
                     <button type="submit" name="login" >SignUP</button> 
                     <div class="form_or">OR</div>
                     <div class="form_social">
                        <a href="" title="Facebook" class="facebook"><i class="fab fa-facebook-f"></i></a>
                        <a href=""  title="Google" class="google"><img src="images/goo.png"></a>
                        <div class="clearfix"></div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
            </div>
      </div>
      </form>
      <!-- Lofin Up Form -->
      <div id="login" class="modal">
         <form class="form-group form_inner login_inner animate " action=""  method="POST" id="form_login" >
            <div class="imgcontainer">
               <span onclick="document.getElementById('login').style.display='none'" class="close" title="Close">&times;</span>
            </div>
            <div class="form_margin">
               <div class="row">
                  <div class="col-md-12  col-xs-12 text-center">
                     <img src="images/l.png" alt="WOW Airline Reviews">
                  </div>
                  <div class="col-md-6"></div>
                  <div class="col-md-6 col-xs-12 padding_remove ">
                     <h5>Sign <span>In</span></h5>
                     <div class="form-group">
                     <input type="text" name="usermail" placeholder="Enter Your Email Or Username" class="form-control"  >
                   </div>
                   <div class="form-group">
                     <input type="password" name="userpassword" placeholder="Enter Your Password" class="form-control" autocomplete="new-password" >
                   </div>
                     <div class="col-md-6 col-xs-8 padding_remove remember ">
                       <span class="forgot_password" onclick="showforgot()">Forgot Password ?</span>
                       <!--  <input type="checkbox" name="" ><span>Remember My Details</span> -->
                     </div>
                     <div class="col-md-6 col-xs-4 padding_remove text-right">
                        <a href="#"  title="Sign In" class="singin" onclick="showlogin1()">Sign Up</a>
                     </div>
                     <div class="clearfix"></div>
                     <button type="submit" name="login" >LOGIN</button> 
                     <div class="form_or">OR</div>
                     <div class="form_social">
                        <a href="" title="Facebook" class="facebook"><i class="fab fa-facebook-f"></i></a>
                        <a href=""  title="Google" class="google"><img src="images/goo.png"></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
            </div>
             </form>
      </div>

      <!-- Forgot Password -->

     <div id="forgot_password" class="modal">
         <form class="form-group form_inner login_inner animate " action=""  method="POST" id="forgot_password" >
            <div class="imgcontainer">
               <span onclick="document.getElementById('forgot_password').style.display='none'" class="close" title="Close">&times;</span>
            </div>
            <div class="form_margin">
               <div class="row">
                  <div class="col-md-12  col-xs-12 text-center">
                     <img src="images/l.png" alt="WOW Airline Reviews">
                  </div>
                  <div class="col-md-1"></div>
                  <div class="col-md-10 col-xs-12 padding_remove ">
                     <h5>Forgot <span>Password</span></h5>
                     <div class="form-group">
                     <input type="text" name="forgot_mail" placeholder="Enter Your Registered Email " class="form-control"  >
                   </div>
                    
                     <div class="clearfix"></div>
                     <button type="submit" name="login" >Send</button> 
                    
                     <div class="clearfix"></div>
                  </div>
                  <div class="col-md-1"></div>
               </div>
            </div>
             </form>
      </div>
     