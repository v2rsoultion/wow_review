<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
                <h1>Emirates <span> Airbus A380</span></h1>
                <p>Home / Search Flight / <a href="" title="Write a review">Emirates Airbus A380 </a></p>
            </div>
        </div>
    </div>
</section>
<!-- Write Review Section -->
<section id="review_write" class="progrebar_list">
    <div class="container">
        <div class="row">
            <div class="col-md-9 padding_remove">
               <div class="top_padding extrap listing_detail">
               <p class="research_today">Flights Reviews of Emirates Airbus A380</p>
               <a href="" title="Edit Review" class="search_rew_a">Edit Search</a>
               <div class="clearfix"></div>
               <div class="sorting_list">
                  <h3>
                     Sort By : 
                     <select>
                        <option>rating</option>
                     </select>
                  </h3>
                  <p>Showing 1–9 of 10 Results</p>
                  <div class="clearfix"></div>
               </div>
            </div>
                <div class="thanks_setion wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
             <div class="clearfix"></div>
                <div class="list_reviewss">
                	<div class="listclass_list">
                        <h3>Clara Bow</h3>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i><span>Reviewed 1 week ago  </span>
                        <p>
                        	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur dolor sit adipiscing elit. sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua
                        </p>
                    </div>
                    <div class="list_class_right">
                    	<a href="" title="like"><i class="far fa-thumbs-up"></i></a>
                    	<a href="" title="share"><i class="fas fa-share-alt"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                 <div class="list_reviewss">
                	<div class="listclass_list">
                        <h3>Clara Bow</h3>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i><span>Reviewed 1 week ago  </span>
                        <p>
                        	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur dolor sit adipiscing elit. sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua
                        </p>
                    </div>
                    <div class="list_class_right">
                    	<a href="" title="like"><i class="far fa-thumbs-up"></i></a>
                    	<a href="" title="share"><i class="fas fa-share-alt"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                 <div class="list_reviewss">
                    <div class="listclass_list">
                        <h3>Clara Bow</h3>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i><span>Reviewed 1 week ago  </span>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur dolor sit adipiscing elit. sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua
                        </p>
                    </div>
                    <div class="list_class_right">
                        <a href="" title="like"><i class="far fa-thumbs-up"></i></a>
                        <a href="" title="share"><i class="fas fa-share-alt"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                 <div class="list_reviewss">
                    <div class="listclass_list">
                        <h3>Clara Bow</h3>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i><span>Reviewed 1 week ago  </span>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur dolor sit adipiscing elit. sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua
                        </p>
                    </div>
                    <div class="list_class_right">
                        <a href="" title="like"><i class="far fa-thumbs-up"></i></a>
                        <a href="" title="share"><i class="fas fa-share-alt"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                 <div class="list_reviewss">
                    <div class="listclass_list">
                        <h3>Clara Bow</h3>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i><span>Reviewed 1 week ago  </span>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur dolor sit adipiscing elit. sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua
                        </p>
                    </div>
                    <div class="list_class_right">
                        <a href="" title="like"><i class="far fa-thumbs-up"></i></a>
                        <a href="" title="share"><i class="fas fa-share-alt"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                 <div class="list_reviewss">
                    <div class="listclass_list">
                        <h3>Clara Bow</h3>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i><span>Reviewed 1 week ago  </span>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur dolor sit adipiscing elit. sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua
                        </p>
                    </div>
                    <div class="list_class_right">
                        <a href="" title="like"><i class="far fa-thumbs-up"></i></a>
                        <a href="" title="share"><i class="fas fa-share-alt"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                </div>
                 <div class="col-md-2"></div>
            <div class="col-md-8">
            <div class="bs-example text-center padding_page">
                <ul class="pagination">
                    <li><a href="#" class="paginaton">«</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#" class="paginaton">»</a></li>
                </ul>
               </div>
                 <div class="col-md-2"></div>
            </div>
            </div>

            <div class="col-md-3 common_side padding_right1 wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                <div class="container1">
                    <img src="images/s20.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>

                <div class="container1">
                    <img src="images/side1.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>
                <div class="container1">
                    <img src="images/side2.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>
                 <div class="container1">
                    <img src="images/s20.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>
                <div class="container1">
                    <img src="images/side2.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
<!-- Include Footer  -->
<?php include('footer.php'); ?>