<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
            <h1>Melbourne To  <span> Perth</span></h1>
            <p>Home / Search Flight /  <a href="" title="Write a review">Melbourne To Perth</a></p>
         </div>
      </div>
   </div>
</section>
<!-- Listing Section -->
<section class="listing_page">
   <div class="container">
      <div class="row">
         <!-- Side Bar -->
         <div class="col-md-3 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
            <div class="listing_side">
               <h4>Rating</h4>
               <form>
                  <img src="images/u.png" alt="border" class="border123">
                  <div class="rattingone">
                    
                      <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                        <label for="styled-checkbox-1">4 &amp; above</label>
                     <img src="images/k2.png" alt="Rating" class="ratingone">
                     <div class="clearfix"></div>
                  </div>
                  <div class="rattingone">
                    <input class="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value1">
                        <label for="styled-checkbox-2">3 &amp; above</label>
                     <img src="images/k1.png" alt="Rating" class="ratingone">
                     <div class="clearfix"></div>
                  </div>
                  <div class="rattingone">
                     <input class="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value1">
                        <label for="styled-checkbox-3">2 &amp; above</label>
                     <img src="images/k3.png" alt="Rating" class="ratingone">
                     <div class="clearfix"></div>
                  </div>
                  <div class="rattingone">
                     <input class="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value1">
                        <label for="styled-checkbox-4">1 &amp; above</label>
                     <img src="images/k4.png" alt="Rating" class="ratingone">
                     <div class="clearfix"></div>
                  </div>
               </form>
            </div>
              <div class="clearfix"></div>
            <div class="listing_side">
               <h4>Airlines</h4>
               <form>
                  <img src="images/u.png" alt="border" class="border123">
                  <div class="rattingone">
                     
                       <input class="styled-checkbox" id="styled-checkbox-5" type="checkbox" value="value1">
                        <label for="styled-checkbox-5">Tigerair</label>

                     <div class="clearfix"></div>
                  </div>
                  <div class="rattingone">
                 
                     <input class="styled-checkbox" id="styled-checkbox-6" type="checkbox" value="value1">
                        <label for="styled-checkbox-6">Emirates</label>
                     <div class="clearfix"></div>
                  </div>
                  <div class="rattingone">
                    
                     <input class="styled-checkbox" id="styled-checkbox-7" type="checkbox" value="value1">
                        <label for="styled-checkbox-7">Qatar airways</label>
                     <div class="clearfix"></div>
                  </div>
                  <div class="rattingone">
                  

                     <input class="styled-checkbox" id="styled-checkbox-8" type="checkbox" value="value1">
                        <label for="styled-checkbox-8">Virgin airways</label>
                     <div class="clearfix"></div>
                  </div>
               </form>
            </div>
              <div class="clearfix"></div>
            <div class="listing_side">
               <h4>Times</h4>
               <p>Outbound (MEL TO PER)</p>
               <h5>4H 10M</h5>
               <h6>4H 10M</h6>
               <div class="clearfix"></div>
               <div class="range_slider">
                 <input id="ex6" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="50"/>
              </div>

              <p>Return (PER TO MEL)</p>
               <h5>4H 10M</h5>
               <h6>4H 10M</h6>
               <div class="clearfix"></div>
               <div class="range_slider">
                 <input id="ex5" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="80"/>
              </div>
            </div>
              <div class="clearfix"></div>
             <div class="listing_side">
               <h4>Duration</h4>
               <p>Under 19H 30M</p>
               <div class="clearfix"></div>
               <div class="range_slider">
                 <input id="ex7" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="30"/>
              </div>
            </div>
              <div class="clearfix"></div>
             <div class="listing_side">
               <h4>Stops</h4>
                 <img src="images/u.png" alt="border" class="border123">
                   <div class="inputBox radion1 " id="check_boxes">
                         <input type="radio" id="one" name="radio-group">
                         <label for="one">Non Stop</label>
                         <input type="radio" id="two" name="radio-group">
                         <label for="two">Upto 1 Stop</label>
                          <input type="radio" id="three" name="radio-group">
                         <label for="three">Upto 2 Stop</label>
                  </div>
                        <div class="clearfix"></div>
                 </div>
                   <div class="clearfix"></div>
             <div class="listing_side one_listing">
               <h4>On Time Rating</h4>
               <p>50%</p>
               <div class="clearfix"></div>
               <div class="range_slider">
                 <input id="ex8" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="50"/>
              </div>
            </div>
              <div class="clearfix"></div>
             <div class="listing_side one_listing">
               <h4>Ground Service</h4>
               <p>60%</p>
               <div class="clearfix"></div>
               <div class="range_slider">
                 <input id="ex9" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="60"/>
              </div>
            </div>
              <div class="clearfix"></div>
             <div class="listing_side xtarpadding">
               <h4>Seat Comfart</h4>
               <p>70%</p>
               <div class="clearfix"></div>
               <div class="range_slider">
                 <input id="ex10" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="70"/>
              </div>
                <div class="clearfix"></div>
            </div>
              <div class="clearfix"></div>

         </div>

         <div class="col-md-9 col-xs-12 remo_padding wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
            <div class="top_padding extrap">
               <p class="research_today">Flights From Melbourne to perth</p>
               <a href="" title="Edit Review" class="search_rew_a">Edit Search</a>
               
               <div class="sorting_list">
                  <h3>
                     Sort By : 
                     <select>
                        <option>rating</option>
                     </select>
                  </h3>
                  <p>Showing 1–9 of 10 Results</p>
                  <div class="clearfix"></div>
               </div>
            </div>
              
            <div class="review_details">
               <div class="review_img">
                  <img src="images/r1.png" title="Tigerair">
               </div>
               <div class="review_left">
                  <div class="inner_left">
                     <h1>8:45 PM -   11:05 PM  </h1>
                     <h2>4h 20m Nonstop</h2>
                     <h3>Tigerair (TR) · Airbus A319</h3>
                  </div>
                  <div class="inner_right">
                     <span>25 Reviews</span>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="far fa-star"></i>
                     <span class="paddingBottom">On Time Rating <br><small>90%</small></span>
                  </div>
                  <div class="clearfix"></div>
                  <a href="" title="See Reviews"><i class="fas fa-eye"></i> See Reviews</a>
                  <a href="" title="Write A Review"><i class="fas fa-edit"></i> Write A Review</a>
                  <a href="" title="Share Now"><i class="fas fa-share-alt"></i> Share Now</a>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="review_details">
               <div class="review_img">
                  <img src="images/r1.png" title="Tigerair">
               </div>
               <div class="review_left">
                  <div class="inner_left">
                     <h1>8:45 PM -   11:05 PM  </h1>
                     <h2>4h 20m Nonstop</h2>
                     <h3>Tigerair (TR) · Airbus A319</h3>
                  </div>
                  <div class="inner_right">
                     <span>25 Reviews</span>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="far fa-star"></i>
                     <span class="paddingBottom">On Time Rating <br><small>90%</small></span>
                  </div>
                  <div class="clearfix"></div>
                  <a href="" title="See Reviews"><i class="fas fa-eye"></i> See Reviews</a>
                  <a href="" title="Write A Review"><i class="fas fa-edit"></i> Write A Review</a>
                  <a href="" title="Share Now"><i class="fas fa-share-alt"></i> Share Now</a>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="review_details">
               <div class="review_img">
                  <img src="images/r1.png" title="Tigerair">
               </div>
               <div class="review_left">
                  <div class="inner_left">
                     <h1>8:45 PM -   11:05 PM  </h1>
                     <h2>4h 20m Nonstop</h2>
                     <h3>Tigerair (TR) · Airbus A319</h3>
                  </div>
                  <div class="inner_right">
                     <span>25 Reviews</span>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="far fa-star"></i>
                     <span class="paddingBottom">On Time Rating <br><small>90%</small></span>
                  </div>
                  <div class="clearfix"></div>
                  <a href="" title="See Reviews"><i class="fas fa-eye"></i> See Reviews</a>
                  <a href="" title="Write A Review"><i class="fas fa-edit"></i> Write A Review</a>
                  <a href="" title="Share Now"><i class="fas fa-share-alt"></i> Share Now</a>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="review_details">
               <div class="review_img">
                  <img src="images/r1.png" title="Tigerair">
               </div>
               <div class="review_left">
                  <div class="inner_left">
                     <h1>8:45 PM -   11:05 PM  </h1>
                     <h2>4h 20m Nonstop</h2>
                     <h3>Tigerair (TR) · Airbus A319</h3>
                  </div>
                  <div class="inner_right">
                     <span>25 Reviews</span>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="far fa-star"></i>
                     <span class="paddingBottom">On Time Rating <br><small>90%</small></span>
                  </div>
                  <div class="clearfix"></div>
                  <a href="" title="See Reviews"><i class="fas fa-eye"></i> See Reviews</a>
                  <a href="" title="Write A Review"><i class="fas fa-edit"></i> Write A Review</a>
                  <a href="" title="Share Now"><i class="fas fa-share-alt"></i> Share Now</a>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="review_details">
               <div class="review_img">
                  <img src="images/r2.png" title="Amirates">
               </div>
               <div class="review_left">
                  <div class="inner_left">
                     <h1>8:45 PM -   11:05 PM  </h1>
                     <h2>4h 20m Nonstop</h2>
                     <h3>Tigerair (TR) · Airbus A319</h3>
                  </div>
                  <div class="inner_right">
                     <span>25 Reviews</span>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="far fa-star"></i>
                     <span class="paddingBottom">On Time Rating <br><small>90%</small></span>
                  </div>
                  <div class="clearfix"></div>
                  <a href="" title="See Reviews"><i class="fas fa-eye"></i> See Reviews</a>
                  <a href="" title="Write A Review"><i class="fas fa-edit"></i> Write A Review</a>
                  <a href="" title="Share Now"><i class="fas fa-share-alt"></i> Share Now</a>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix"></div>
            </div>
             <div class="review_details">
               <div class="review_img">
                  <img src="images/r2.png" title="Amirates">
               </div>
               <div class="review_left">
                  <div class="inner_left">
                     <h1>8:45 PM -   11:05 PM  </h1>
                     <h2>4h 20m Nonstop</h2>
                     <h3>Tigerair (TR) · Airbus A319</h3>
                  </div>
                  <div class="inner_right">
                     <span>25 Reviews</span>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="far fa-star"></i>
                     <span class="paddingBottom">On Time Rating <br><small>90%</small></span>
                  </div>
                  <div class="clearfix"></div>
                  <a href="" title="See Reviews"><i class="fas fa-eye"></i> See Reviews</a>
                  <a href="" title="Write A Review"><i class="fas fa-edit"></i> Write A Review</a>
                  <a href="" title="Share Now"><i class="fas fa-share-alt"></i> Share Now</a>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix"></div>
            </div>
              <div class="clearfix"></div>
            <div class="col-md-2"></div>
              <div class="col-md-8">
            <div class="bs-example text-center">
                <ul class="pagination">
                    <li><a href="#" class="paginaton">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#" class="paginaton">&raquo;</a></li>
                </ul>
               </div>
            </div>
             <div class="col-md-2"></div>
              
         </div>
         
      </div>
   </div>
</section>
<!-- Footer -->
<?php include('footer.php'); ?>