<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
                <h1>Emirates <span>  Airbus A380 </span></h1>
                <p>Home / Search Flight / <a href="" title="Write a review">Emirates Airbus A380 </a></p>
            </div>
        </div>
    </div>
</section>
<!-- Write Review Section -->
<section id="review_write" class="progrebar_list">
    <div class="container">
        <div class="row">
            <div class="col-md-9 padding_remove">
                <div class="main_review_write listing_single wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <img src="images/im.png" alt="Emirates">
                    <div class="review_write_float">
                        <h3>Emirates · Airbus A380</h3>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i><span>1235 Reviews</span>
                        <p>4h 30m Nonstop</p>
                        <button>Facilities</button>
                    </div>
                    <div class="list_float">
                        <a href="#" title="Add Review"><i class="fas fa-edit"></i> Add Review</a>
                        <a href="#" title="Share"><i class="fas fa-share-alt"></i> Share</a>
                        <div class="clearfix"></div>
                        <p>On Time Rating<br>
                            <span>90%</span>
                        </p>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="list_flight_review">
                    <h5>Flight Reviews</h5>
                </div>
                <div class="flight_progress">
                    <div class="col-md-2 padding_progreass text-left">
                        <h3>Excellent</h3>
                        <h3>Very good</h3>
                        <h3>Average</h3>
                        <h3>Poor</h3>
                        <h3>Terrible</h3>
                    </div>
                    <div class="col-md-8">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="80" style="max-width:80%">
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="60" style="max-width:60%">
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="40" style="max-width:40%">
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="30" style="max-width:30%">
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="10" style="max-width:10%">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 padding_remove text-center">
                    	<div class="progress_value">
                    		<p>500 <img src="images/flight.png" alt="Value Of Progress"></p>
                    		<p>250 <img src="images/flight.png" alt="Value Of Progress"></p>
                    		<p>150 <img src="images/flight.png" alt="Value Of Progress"></p>
                    		<p>24 <img src="images/flight.png" alt="Value Of Progress"></p>
                    		<p>3 <img src="images/flight.png" alt="Value Of Progress"></p>
                    	</div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="list_flight_review">
                    <h6>Other Alternate Times</h6>
                </div>
                <div class="review_details wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <div class="review_img">
                        <img src="images/r1.png" title="Tigerair">
                    </div>
                    <div class="review_left">
                        <div class="inner_left">
                            <h1>8:45 PM - 11:05 PM </h1>
                            <h2>4h 20m Nonstop</h2>
                            <h3>Tigerair (TR) · Airbus A319</h3>
                        </div>
                        <div class="inner_right">
                            <span>25 Reviews</span>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="far fa-star"></i>
                            <span class="paddingBottom">On Time Rating <br><small>90%</small></span>
                        </div>
                        <div class="clearfix"></div>
                        <a href="" title="See Reviews"><i class="fas fa-eye"></i> See Reviews</a>
                        <a href="" title="Write A Review"><i class="fas fa-edit"></i> Write A Review</a>
                        <a href="" title="Share Now"><i class="fas fa-share-alt"></i> Share Now</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="review_details wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <div class="review_img">
                        <img src="images/r2.png" title="Tigerair">
                    </div>
                    <div class="review_left">
                        <div class="inner_left">
                            <h1>8:45 PM - 11:05 PM </h1>
                            <h2>4h 20m Nonstop</h2>
                            <h3>Tigerair (TR) · Airbus A319</h3>
                        </div>
                        <div class="inner_right">
                            <span>25 Reviews</span>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="far fa-star"></i>
                            <span class="paddingBottom">On Time Rating <br><small>90%</small></span>
                        </div>
                        <div class="clearfix"></div>
                        <a href="" title="See Reviews"><i class="fas fa-eye"></i> See Reviews</a>
                        <a href="" title="Write A Review"><i class="fas fa-edit"></i> Write A Review</a>
                        <a href="" title="Share Now"><i class="fas fa-share-alt"></i> Share Now</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="thanks_setion wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">

                    <div class="col-md-3 padding_remove ">
                        <div class="peer1">
                            <div class="c_padding">
                                <p>Punctuality</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="c_padding">
                                <p>Seat comfort</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="c_padding">
                                <p>Seat comfort</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 padding_remove ">
                        <div class="peer1 peer2">
                            <div class="c_padding">
                                <p>Ground service</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="c_padding">
                                <p>Cabin Staff service</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="c_padding">
                                <p>Entertainment</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 padding_remove ">
                        <div class="peer1">
                            <div class="c_padding">
                                <p>Entertainment</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="c_padding">
                                <p>Seat comfort</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="c_padding">
                                <p>Value for money</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 padding_remove ">
                        <div class="peer1  moreperr">
                            <div class="c_padding">
                                <p>Cleanliness & hygiene</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="c_padding">
                                <p>Seat comfort</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="c_padding">
                                <p>Seat comfort</p>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <!-- Close Service Section -->
                    <div class="list_flight_review">
                    	<br>
                    <h6>Reviews</h6>
                </div>
                <div class="list_reviewss">
                	<div class="listclass_list">
                        <h3>Clara Bow</h3>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i><span>Reviewed 1 week ago  </span>
                        <p>
                        	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur dolor sit adipiscing elit. sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua
                        </p>
                    </div>
                    <div class="list_class_right">
                    	<a href="" title="like"><i class="far fa-thumbs-up"></i></a>
                    	<a href="" title="share"><i class="fas fa-share-alt"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                 <div class="list_reviewss">
                	<div class="listclass_list">
                        <h3>Clara Bow</h3>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i><span>Reviewed 1 week ago  </span>
                        <p>
                        	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur dolor sit adipiscing elit. sed do eio usmod tem por incididunt sed do eio usmod  ut labore et dolore magna aliqua
                        </p>
                    </div>
                    <div class="list_class_right">
                    	<a href="" title="like"><i class="far fa-thumbs-up"></i></a>
                    	<a href="" title="share"><i class="fas fa-share-alt"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                </div>
                 <div class="col-md-2"></div>
			<div class="col-md-8">
            <div class="bs-example text-center padding_page">
                <ul class="pagination">
                    <li><a href="#" class="paginaton">«</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#" class="paginaton">»</a></li>
                </ul>
               </div>
                 <div class="col-md-2"></div>
            </div>
                <!-- Thanks Section  -->
            </div>

            <div class="col-md-3 common_side padding_right1 wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                <div class="container1">
                    <img src="images/s20.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>

                <div class="container1">
                    <img src="images/side1.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>
                <div class="container1">
                    <img src="images/side2.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>
                 <div class="container1">
                    <img src="images/s20.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>
                <div class="container1">
                    <img src="images/side5.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
<!-- Include Footer  -->
<?php include('footer.php'); ?>