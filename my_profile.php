<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
                <h1>My <span> Profile</span></h1>
                <p>Home / <a href="" title="My Profile">My Profile</a></p>
            </div>
        </div>
    </div>
</section>

<!-- Section for Profile Body  -->
<section class="edit_profile">
    <div class="container">
        <div class="row">
            <div class="col-md-12 top_profile ">
                <div class="col-md-5 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <h4>Johnson Boaz</h4>
                    <p>These are your personal account details</p>
                    <a href="edit_profile.php" title="Edit Profile">Edit Profile</a>
                </div>
                <div class="col-md-7 padding_remove borderbottom wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <div class="skew_class">
                        <h3>Points Table</h3>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="95" style="max-width:95%">
                            </div>
                        </div>
                        <h5>200 Point </h5>
                        <h6>500 Point</h6>
                        <div class="clearfix"></div>
                        <button title="View Points Table">View Points Table</button>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 my_account">
                <div class="col-md-6 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <h4>About Me</h4>
                    <br>
                    <ul>
                        <li><strong>First Name : </strong><span>Johnson</span></li>
                        <div class="clearfix"></div>
                        <li><strong>last Name : </strong><span>Boaz</span></li>
                        <div class="clearfix"></div>
                        <li><strong>Phone : </strong><span><a href="tel:0123-456-789" title="Click To Call">0123-456-789</a></span></li>
                        <div class="clearfix"></div>
                        <li><strong>Address : </strong><span>123 Lorem ipsum dolor
													sit amet.</span></li>
                        <div class="clearfix"></div>
                        <li><strong>Email : </strong><span><a href="mailto:info@Loremipsum.com" title="Send Mail us"> info@Loremipsum.com</a></span></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                <div class="col-md-6 myAccount wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <h4>My Account</h4>
                 <br>
                    <ul>
                        <a href="changepassword.php" title="Change Password">
                            <li>Change Password</li>
                        </a>
                        <a href="my_reviews.php" title="My Reviews">
                            <li>My Reviews</li>
                        </a>
                        <a href="my_reviews.php" title="My Likes Reviews">
                            <li>My Likes Reviews</li>
                        </a>
                         <a href="frequent_membership.php" title="Frequent Flyer Membership">
                                <li>Frequent Flyer Membership</li>
                            </a>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<?php include('footer.php'); ?>