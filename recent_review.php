<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
				<h1>Recent <span> Reviews</span></h1>
				<p>Home / <a href="" title="Write a review">Recent reviews</a></p>
			</div>
		</div>
	</div>
</section>
<!-- Write Review Section -->
<section id="review_write">
	<div class="container ">
		<div class="row recent_padding">
			<div class="col-md-9 padding_remove">
				
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			
			<div class="clearfix"></div>
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			
			<div class="clearfix"></div>
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			
			<div class="clearfix"></div>
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			
			<div class="col-md-4 recent_margin  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<img src="images/recent.jpg" class="img-responsive" alt="Tigerair 432 - Boeing 737">
				<div class="recent_review">
                        <div class="padding_inside">
                            <span>Tigerair 432 - Boeing 737</span>
                            <h3>MEL to PER</h3>
                            <p>Lorem ipsum dolor sit amet, consec tetur adi piscing elit, sed do eiusmod tempor aliqua Ut enim.</p>
                        </div>
                        <div class="inside_div h4div">
                            <p>Simmy Tikota
                                <span>Nov 30, 2017</span>
                            </p>
                            <h4>
                                <span>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    		</span>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="social_sharing">
                            <h4><a href="" title="like"><i class="fas fa-thumbs-up"></i> Like</a></h4>
                            <h5><a href="" title="share"><i class="fas fa-share-alt"></i> Share</a></h5>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			</div>
			
			<div class="clearfix"></div>


			</div>
			<div class="col-md-3  padding_right1 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
			<div class="container1">
			  <img src="images/side2.jpg" alt="Avatar" class="image">
			  <div class="overlay">
			    <div class="text">
			    	<h2>Emirates</h2>
			    	<span>Airbus A380</span>
			    </div>
			  </div>
			</div>
			<div class="container1">
			  <img src="images/side1.jpg" alt="Avatar" class="image">
			  <div class="overlay">
			   <div class="text">
			    	<h2>Emirates</h2>
			    	<span>Airbus A380</span>
			    </div>
			  </div>
			</div>
			<div class="container1">
			  <img src="images/side2.jpg" alt="Avatar" class="image">
			  <div class="overlay">
			   <div class="text">
			    	<h2>Emirates</h2>
			    	<span>Airbus A380</span>
			    </div>
			  </div>
			</div>
			<div class="container1">
			  <img src="images/side5.jpg" alt="Avatar" class="image">
			  <div class="overlay">
			   <div class="text">
			    	<h2>Emirates</h2>
			    	<span>Airbus A380</span>
			    </div>
			  </div>
			</div>
			
		</div>
		</div>
	</div>
</section>
<!-- Include Footer  -->
<?php include('footer.php'); ?>