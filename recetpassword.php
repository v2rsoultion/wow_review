<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
				<h1>Reset <span> Password</span></h1>
				<p>Home / <a href="" title="Write a review">Reset Password</a></p>
			</div>
		</div>
	</div>
</section>
    <section class="edit_profile" id="recetpasswordd">
        <div class="container">
            <div class="row">
               <div class="col-md-8  col-md-offset-2 my_account">
                        <div class="col-md-12 col-xs-12  padding_remove ">
                        	 <form class="" action="" method="" id="changepassword" autocomplete="off">
                            <div class="form-group">
                                <input type="password" name="currentpassword" placeholder="Enter Your Current Password" class="form-control" autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <input type="password" name="newpassword" placeholder="Enter Your New Password" class="form-control" autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <input type="password" name="retypepassword" placeholder="Enter Your Re-type Password" class="form-control" autocomplete="new-password">
                            </div>
                            <button type="submit" title="Change Password" name="changepassword">Reset Password</button>
                            <div class="clearfix"></div>
                         </form>
                    </div>
                  </div>
                </div>
            </div>
        </section>