var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "write_review1.php"
    })
    .when("/red", {
        templateUrl : "write_review.php"
    })
    .when("/green", {
        templateUrl : "recent_review.php"
    })
   
});