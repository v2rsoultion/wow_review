<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
                <h1>Lorem<span>  ipsum</span></h1>
                <p>Home / Blog <a href="" title="Blog">/ Lorem ipsum</a ></p>
         </div>
      </div>
   </div>
</section>
<section id="">
   <div class="container">
      <div class="row" id="common_blog">
         <div class="col-md-8 left_blog">
            <img src="images/b0.jpg" alt="Blog" class="wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
            <h4 class="wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">Lorem ipsum dolor sit amet, consectetur elit,</h4>
            <div class="love_likes wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
               <span><i class="change fas fa-heart" title="Likes"></i>20 Likes </span><span><i class="fas fa-comments" title="Comments"></i> 30 Comments</span><span><i class="fas fa-calendar-alt" title="Post Date"></i>June 13, 2018</span><span class="sharing_blog"> <i class="fas fa-share-alt" title="Share"></i>Share</span>
               <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco lab oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolorelor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
               </p>
                 <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco lab oris nisi ut aliquip ex ea commodo consequat.
               </p>

                 <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco lab oris nisi ut aliquip ex ea commodo consequat.
               </p>
              
               <div class="blockquote-box clearfix">
                <p>
                   “ Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nos
                     trud exercit ation ullamco lab oris nisi ut aliquip ex ea commodo consequat. ”
                </p>
               </div>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco lab oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolorelor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
               </p>
               <div class="col-md-6 padding_blg ">
                  <img src="images/b0.jpg" alt="Related Post" class="blog_imagess">
               </div>
               <div class="col-md-6 padd_riht">
                   <img src="images/b1.jpg" alt="Related Post" class="blog_imagess">
               </div>
               <div class="clearfix"></div>
                 <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco lab oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolorelor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
               </p>
            </div>
            <div class="user_comments">
               <h4>User Comments</h4>
               <img src="images/bdr1.png" title="border" alt="border" class="margin_asset1">
            </div>
            <div class="comm_blog">
              <div class="col-md-2 padding_blg ">
                  <img src="images/john.jpg" alt="Post By Admin" class="without_radius">
               </div>
               <div class="col-md-10 back_blog padding_remove">
                  <h3>Lorem John</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
               </div>
                <div class="clearfix"></div>
            </div>

             <div class="comm_blog">
              <div class="col-md-2 padding_blg ">
                  <img src="images/john.jpg" alt="Post By Admin" class="without_radius">
               </div>
               <div class="col-md-10 back_blog padding_remove">
                  <h3>Lorem John</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
               </div>
                <div class="clearfix"></div>
            </div>
             <div class="user_comments">
               <h4>Post Comment</h4>
               <img src="images/bdr1.png" title="border" alt="border" class="margin_asset1">
            </div>
             <div class="comm_blog">
              <div class="col-md-10 padding_remove">
                 <textarea placeholder="Post Comment"></textarea>
              </div>
               <div class="col-md-2 padding_remove">
                  <button type="submit"><i class="fas fa-envelope"></i><br>Submit</button>
               </div>
                <div class="clearfix"></div>
            </div>

         
         </div>
         <!-- Side Bar  -->
         <div class="col-md-4">
            <div class="blog_sidebar">
               <h4>Search</h4>
               <img src="images/bdr1.png" title="border" alt="border"> 
               <form class="" role="search">
                  <div class="input-group add-on wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
                     <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
                     <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                     </div>
                  </div>
               </form>
               <h4>Recent Posts</h4>
               <img src="images/bdr1.png" title="border" alt="border" class="margin_asset"> 
               <div class="recetpost wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
                  <ul>
                     <li>
                        <a href="" title="Sed ut perspiciatis und natus accusantium">
                           <img src="images/re.jpg" alt="Recent Post" >
                           <h3>
                              Sed ut perspiciatis und
                              natus accusantium.
                              <span>Jul 14, 2017   /   0 Comment</span>
                           </h3>
                        </a>
                    <div class="clearfix"></div>
                    </li>
                    </ul>
            </div>
            <div class="recetpost wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" ">
                  <ul>
                     <li>
                        <a href=" " title="Sed ut perspiciatis und natus accusantium ">
                           <img src="images/re.jpg " alt="Recent Post " >
                           <h3>
                              Sed ut perspiciatis und
                              natus accusantium.
                              <span>Jul 14, 2017   /   0 Comment</span>
                           </h3>
                        </a>
                        <div class="clearfix "></div>
                     </li>
                  </ul>
               </div>
               <div class="recetpost wow fadeInUp " data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 "">
                <ul>
                    <li>
                        <a href="" title="Sed ut perspiciatis und natus accusantium">
                           <img src="images/re.jpg" alt="Recent Post" >
                           <h3>
                              Sed ut perspiciatis und
                              natus accusantium.
                              <span>Jul 14, 2017   /   0 Comment</span>
                           </h3>
                           <div class="clearfix"></div>
                        </a>
                    </li>
                </ul>
            </div>
            <h4>Recent Comments</h4>
            <img src="images/bdr1.png" title="border" alt="border" class="margin_asset">
            <div class="recetpost wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" ">
                  <p><b>Lorem ipsum dolor sit amet,</b> consectetur adipiscing elit sed do you incididunt ut labore </p>
               </div>
               <div class="recetpost wow fadeInUp " data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 "">
                <p><b>Lorem ipsum dolor sit amet,</b> consectetur adipiscing elit sed do you incididunt ut labore </p>
            </div>
            <div class="recetpost wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" ">
                  <p><b>Lorem ipsum dolor sit amet,</b> consectetur adipiscing elit sed do you incididunt ut labore </p>
               </div>
               <div class="recetpost wow fadeInUp " data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 "">
                <p><b>Lorem ipsum dolor sit amet,</b> consectetur adipiscing elit sed do you incididunt ut labore </p>
            </div>
            <h4>Blog Categories</h4>
            <img src="images/bdr1.png" title="border" alt="border" class="margin_asset">
            <div class="blog_cat wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" ">
                  <a href=" " title="Aircraft ">
                     <h3></h3>
                     <p>Aircraft</p>
                     <span><i class="fas fa-angle-right "></i></span>
                  </a>
                  <div class="clearfix "></div>
               </div>
               <div class="blog_cat wow fadeInUp " data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 "">
                <a href="" title="Travel">
                    <h3></h3>
                    <p>Travel</p>
                    <span><i class="fas fa-angle-right"></i></span>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="blog_cat wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" ">
                  <a href=" " title="Airport ">
                     <h3></h3>
                     <p>Airport</p>
                     <span><i class="fas fa-angle-right "></i></span>
                  </a>
                  <div class="clearfix "></div>
               </div>
               <div class="blog_cat wow fadeInUp " data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 "">
                <a href="" title="Travel Tips">
                    <h3></h3>
                    <p>Travel Tips</p>
                    <span><i class="fas fa-angle-right"></i></span>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="blog_cat wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" ">
                  <a href=" " title="Destinations ">
                     <h3></h3>
                     <p>Destinations</p>
                     <span><i class="fas fa-angle-right "></i></span>
                  </a>
                  <div class="clearfix "></div>
               </div>
               <div class="blog_cat wow fadeInUp " data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 "">
                <a href="" title="Ground">
                    <h3></h3>
                    <p>Ground</p>
                    <span><i class="fas fa-angle-right"></i></span>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="blog_cat wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" ">
                  <a href=" " title="Inflight ">
                     <h3></h3>
                     <p>Inflight</p>
                     <span><i class="fas fa-angle-right "></i></span>
                  </a>
                  <div class="clearfix "></div>
               </div>
               <div class="blog_cat wow fadeInUp " data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 "">
                <a href="" title="Uncategorized">
                    <h3></h3>
                    <p>Uncategorized</p>
                    <span><i class="fas fa-angle-right"></i></span>
                </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>
<?php include('footer.php'); ?>