<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
				<h1>Tigerair 432 - Boeing 737</h1>
				<p>Home / <a href="my_reviews.php" title="My Reviews">My Reviews</a> / Tigerair 432 - Boeing 737</p>
			</div>
		</div>
	</div>
</section>
<section class="edit_profile" id="detailspage_flight">
    <div class="container">
        <div class="row">
            <div class="col-md-12 my_account" >
                <div class="col-md-6 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <h4>Tigerair 432 - Boeing 737</h4>
                    <br>
                    <ul class="flight_detailsl">
                        <li><strong>User Name : </strong><span>Johnson Boaz</span></li>
                        <div class="clearfix"></div>
                        <li><strong>flight number : </strong><span><a href="#" title="	Boeing 767-300">Boeing 767-300</a></span></li>
                        <div class="clearfix"></div>
                        <li><strong>source and destination : </strong><span>MEL TO PER</span></li>
                        <div class="clearfix"></div>
                        <li><strong>date : </strong><span>Nov 30, 2017</span></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                   <div class="col-md-6 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <h4>FLight Reviews</h4>
                    <br>
                    <ul id="flight_detailsl">
                       <li>
	                       	<p>
                            <i class="fa fa-star " aria-hidden="true "></i>
                              <i class="far fa-star "></i>
                              <i class="far fa-star "></i>
                              <i class="far fa-star "></i>
                              <i class="far fa-star "></i>
	                        </p>
                      </li>
                        <li>
	                       	<p>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                             <i class="far fa-star "></i>
                             <i class="far fa-star "></i>
                            <i class="far fa-star "></i>
	                        </p>
                      </li>
                        <li>
	                       	<p>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                              <i class="fa fa-star " aria-hidden="true "></i>
                              <i class="far fa-star "></i>
                            <i class="far fa-star "></i>
	                        </p>
                      </li>
                        <li>
	                       	<p>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="far fa-star "></i>
	                        </p>
                      </li>
                        <li>
	                       	<p>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
	                        </p>
                      </li>
                    </ul>
                </div>

            </div>
             <div class="col-md-12 my_account">
                <div class="col-md-6 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <h4>Tigerair 432 - Boeing 737</h4>
                    <br>
                    <ul class="flight_detailsl">
                        <li><strong>User Name : </strong><span>Johnson Boaz</span></li>
                        <div class="clearfix"></div>
                        <li><strong>flight number : </strong><span><a href="#" title="	Boeing 767-300">Boeing 767-300</a></span></li>
                        <div class="clearfix"></div>
                        <li><strong>source and destination : </strong><span>MEL TO PER</span></li>
                        <div class="clearfix"></div>
                        <li><strong>date : </strong><span>Nov 30, 2017</span></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                   <div class="col-md-6 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <h4>FLight Reviews</h4>
                    <br>
                    <ul id="flight_detailsl">
                       <li>
	                       	<p>
                            <i class="fa fa-star " aria-hidden="true "></i>
                              <i class="far fa-star "></i>
                              <i class="far fa-star "></i>
                              <i class="far fa-star "></i>
                              <i class="far fa-star "></i>
	                        </p>
                      </li>
                        <li>
	                       	<p>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                             <i class="far fa-star "></i>
                             <i class="far fa-star "></i>
                            <i class="far fa-star "></i>
	                        </p>
                      </li>
                        <li>
	                       	<p>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                              <i class="fa fa-star " aria-hidden="true "></i>
                              <i class="far fa-star "></i>
                            <i class="far fa-star "></i>
	                        </p>
                      </li>
                        <li>
	                       	<p>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="far fa-star "></i>
	                        </p>
                      </li>
                        <li>
	                       	<p>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
                            <i class="fa fa-star " aria-hidden="true "></i>
	                        </p>
                      </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>