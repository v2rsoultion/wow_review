<?php 
error_reporting(0);
$servername = "localhost";
$username = "v2rteste_wow_tem";
$password = "v2r@123!";
$dbname = "v2rteste_wow_temp";

$conn = new mysqli($servername, $username, $password, $dbname);

if(isset($_POST['submit']))
{

 $ins="insert into data_wow set email='".$_POST['email']."',departing='".$_POST['departing']."',arrival='".$_POST['arrival']."',airline='".$_POST['airline']."',flight='".$_POST['flight']."',journey1='".$_POST['journey1']."',seatnumber='".$_POST['seatnumber']."',reviewsubject='".$_POST['reviewsubject']."',yourreview='".$_POST['yourreview']."',punctuality_rating='".$_POST['punctuality_rating']."',service_rating='".$_POST['service_rating']."',comfort_rating='".$_POST['comfort_rating']."',staff_rating='".$_POST['staff_rating']."',beverages_rating='".$_POST['beverages_rating']."',inflight_rating='".$_POST['inflight_rating']."',money_rating='".$_POST['money_rating']."',amenities_rating='".$_POST['amenities_rating']."',overall_rating='".$_POST['overall_rating']."',class_rating='".$_POST['class_rating']."',children='".$_POST['children']."'";


if ($conn->query($ins) === TRUE) {
    $msg= "Thank You for submitting your review.";
} else {
    $msg= "Error: " . $ins . "<br>" . $conn->error;
}

	
}

$conn->close();

?>
<!DOCTYPE html>
<html>
   <head>
      <title>WOW Review</title>
      <meta charset="UTF-8">
<link rel="shortcut icon" type="image/png" href="images/fav.png" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="stylesheet/wow_review.css"/>
      <link rel="stylesheet" type="text/css" href="stylesheet/bootstrap.min.css"/>
      <link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
       <link rel="stylesheet" type="text/css" href="stylesheet/jquery-ui.css">
      <link rel="stylesheet" type="text/css" href="stylesheet/responsive.css">
   </head>
 <!-- Header Section -->
   <header>
      <section class="header-top">
         <div class="container-fluid">
         	<!-- Site logo -->
            <div class="col-md-4 logo text-center wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
               <a href="index.php" title="logo">
               <img src="images/logo1.png" alt="logo" >
               </a>
            </div>
          <!-- Site Navigation -->
            <div class="col-md-5 wow_menu text-center">
	               <nav class="navbar navbar-inverse">
		                  <ul class="nav navbar-nav">
		                     <li><a href="" title="Write A Review">Write A Review</a></li>
		                     <li><a href="" title="Search Reviews">Search Reviews</a></li>
		                     <li><a href="" title="Recent Reviews">Recent Reviews</a></li>
		                  </ul>
		            </div>
		            <!-- Login Section -->
		            <div class="col-md-3 padding_remove wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				            <ul class="login_process">
					            <li><a href="" title="login">Log In</a></li> I
					            <li><a href="" title="Sign Up">Sign Up</a></li>
				            </ul>
				            <!-- Social Media Bar -->
				            <div class="social_media">
					            <ul class="">
					            <li><a href="" title="android"><i class="fab fa-android"></i></a></li>
					            I
					            <li><a href="" title="apple"><i class="fab fa-apple"></i></a></li>
				            </ul>
				         </div>
		            </div>
		            <div class="clearfix"></div>
	            </nav>
         </div>
         </div>
      </section>
   </header>
<!-- End Header Section  -->
<!-- Common Section -->
<section id="common_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
				<h1>Submit <span> Your Review</span></h1>
				<!-- <p>Home / <a href="" title="Write a review">Write a review</a></p> -->
			</div>
		</div>
	</div>
</section>

<!-- Search Flight Section -->
<section id="search_flight">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 search_shadow">
				<form class="" action="" method="post" id="">
				<div class="section form-label  form-css-label">
        <?php if($msg!=""){  ?>
					<div class="alert alert-success">
					  <strong><?php echo @$msg; ?>.</strong>
					</div>
					<?php } ?>
				  			<div class="text-danger"></div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="email" name="email" type="email" autocomplete="off" required />
							      <label for="email">Email address*</label>
							    </fieldset>
							</div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="departing" name="departing" type="text" autocomplete="off" required />
							      <label for="departing">Departing Station*</label>
							    </fieldset>
							</div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="arrival" name="arrival" type="text" autocomplete="off" required />
							      <label for="arrival">Arrival Station*</label>
							    </fieldset>
							  </div>
							  <div class="clearfix"></div>
							 <div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="airline" name="airline" type="text" autocomplete="off" required />
							      <label for="airline">Airline Name*</label>
							    </fieldset>
							</div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>

							      <input id="flight" name="flight" type="text" autocomplete="off" required />
							      <label for="flight">Flight Number*</label>
							    </fieldset>
							</div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="journey" name="journey1" type="text"  required />
							      <label for="journey">Date Of Journey*</label>
							    </fieldset>
							</div>


							<div class="clearfix"></div>
							    <div class="col-md-4 col-xs-12 form-group">
							  
							    <fieldset>
							      <input id="seatnumber" name="seatnumber" type="text" autocomplete="off" required />
							      <label for="seatnumber">Seat Number*</label>
							    </fieldset>
							</div>
							
								<div class="clearfix"></div>
							</div>
							<div class="">
								<br>
								<div class="col-md-12"><b style="font-size: 18px;">Note:- </b><br><span style="font-size: 17px;     font-family: 'poppinsregular'; line-height:30px;     color: #56676b;">1 Star means Poor<br>  2 Star means Average <br>3 Star means Satisfactory<br> 4 Star means Good<br> 5 Star means Excellent</span></div>
								<br>
							<div class="col-md-3 col-xs-12 review_ratingcl">
									
								<br>
							  <label for="rating" class="labelll">Punctuality Rating *</label>
							  <p>
								  <input type="radio" id="star1" name="punctuality_rating" required="required" value="1">
								  <label for="star1">1 Star</label>
								  <br>
								  <input type="radio" id="star2" name="punctuality_rating" required="required" value="2">
								  <label for="star2">2 Star</label>
								  <br>
								  <input type="radio" id="star3" name="punctuality_rating" required="required" value="3">
								  <label for="star3">3 Star</label>
								   <br>
								  <input type="radio" id="star4" name="punctuality_rating" required="required" value="4">
								  <label for="star4">4 Star</label>
								   <br>
								  <input type="radio" id="star5" name="punctuality_rating" required="required" value="5">
								  <label for="star5">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Ground Service Rating *</label>
							  <p>
								  <input type="radio" id="star11" name="service_rating"  required="required"   value="1"  >
								  <label for="star11">1 Star</label>
								  <br>
								  <input type="radio" id="star22" name="service_rating" required="required"    value="2" >
								  <label for="star22">2 Star</label>
								  <br>
								  <input type="radio" id="star32" name="service_rating" required="required"  value="3"  >
								  <label for="star32">3 Star</label>
								   <br>
								  <input type="radio" id="star44" name="service_rating" required="required"   value="4" >
								  <label for="star44">4 Star</label>
								   <br>
								  <input type="radio" id="star55" name="service_rating" required="required"   value="5" >
								  <label for="star55">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Seat Comfort Rating*</label>
							  <p>
								  <input type="radio" id="star111" name="comfort_rating"  required="required"   value="1" >
								  <label for="star111">1 Star</label>
								  <br>
								  <input type="radio" id="star222" name="comfort_rating" required="required"   value="2" >
								  <label for="star222">2 Star</label>
								  <br>
								  <input type="radio" id="star323" name="comfort_rating" required="required"  value="3"  >
								  <label for="star323">3 Star</label>
								   <br>
								  <input type="radio" id="star444" name="comfort_rating" required="required"  value="4" >
								  <label for="star444">4 Star</label>
								   <br>
								  <input type="radio" id="star555" name="comfort_rating" required="required"  value="5" >
								  <label for="star555">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Cabin Staff Service Rating*</label>
							  <p>
								  <input type="radio" id="star1111" name="staff_rating"  required="required"  value="1" >
								  <label for="star1111">1 Star</label>
								  <br>
								  <input type="radio" id="star2222" name="staff_rating" required="required"   value="2">
								  <label for="star2222">2 Star</label>
								  <br>
								  <input type="radio" id="star3233" name="staff_rating" required="required"   value="3">
								  <label for="star3233">3 Star</label>
								   <br>
								  <input type="radio" id="star4444" name="staff_rating" required="required"   value="4">
								  <label for="star4444">4 Star</label>
								   <br>
								  <input type="radio" id="star5555" name="staff_rating" required="required"   value="5">
								  <label for="star5555">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Food & Beverages Rating *</label>
							  <p>
								  <input type="radio" id="star11111" name="beverages_rating"  required="required"  value="1" >
								  <label for="star11111">1 Star</label>
								  <br>
								  <input type="radio" id="star22222" name="beverages_rating"  required="required"  value="2">
								  <label for="star22222">2 Star</label>
								  <br>
								  <input type="radio" id="star32333" name="beverages_rating" required="required"  value="3">
								  <label for="star32333">3 Star</label>
								   <br>
								  <input type="radio" id="star44444" name="beverages_rating" required="required"  value="4">
								  <label for="star44444">4 Star</label>
								   <br>
								  <input type="radio" id="star55555" name="beverages_rating" required="required" value="5"  >
								  <label for="star55555">5 Star</label>
								</p>
							</div>

							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Inflight Entertainment Rating*</label>
							  <p>
								  <input type="radio" id="star6" name="inflight_rating"  required="required" value="1">
								  <label for="star6">1 Star</label>
								  <br>
								  <input type="radio" id="star61" name="inflight_rating" required="required" value="2">
								  <label for="star61">2 Star</label>
								  <br>
								  <input type="radio" id="star62" name="inflight_rating" required="required" value="3">
								  <label for="star63">3 Star</label>
								   <br>
								  <input type="radio" id="star64" name="inflight_rating" required="required" value="4">
								  <label for="star64">4 Star</label>
								   <br>
								  <input type="radio" id="star65" name="inflight_rating" required="required" value="5">
								  <label for="star65">5 Star</label>
								</p>
							</div>


							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Value For Money Rating*</label>
							  <p>
								  <input type="radio" id="star41" name="money_rating"  required="required" value="1">
								  <label for="star41">1 Star</label>
								  <br>
								  <input type="radio" id="star41" name="money_rating" required="required" value="2">
								  <label for="star41">2 Star</label>
								  <br>
								  <input type="radio" id="star42" name="money_rating" required="required" value="3">
								  <label for="star42">3 Star</label>
								   <br>
								  <input type="radio" id="star43" name="money_rating" required="required" value="4">
								  <label for="star43">4 Star</label>
								   <br>
								  <input type="radio" id="star44" name="money_rating" required="required" value="5">
								  <label for="star44">5 Star</label>
								</p>
							</div>

							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Amenities Rating*</label>
							  <p>
								  <input type="radio" id="star7" name="amenities_rating"  required="required" value="1" >
								  <label for="star7">1 Star</label>
								  <br>
								  <input type="radio" id="star71" name="amenities_rating" required="required" value="2">
								  <label for="star71">2 Star</label>
								  <br>
								  <input type="radio" id="star72" name="amenities_rating" required="required" value="3">
								  <label for="star72">3 Star</label>
								   <br>
								  <input type="radio" id="star73" name="amenities_rating" required="required" value="4">
								  <label for="star73">4 Star</label>
								   <br>
								  <input type="radio" id="star75" name="amenities_rating" required="required" value="5">
								  <label for="star75">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Overall Rating* </label>
							  <p>
								  <input type="radio" id="star81" name="overall_rating"  required="required" value="1">
								  <label for="star81">1 Star</label>
								  <br>
								  <input type="radio" id="star82" name="overall_rating" required="required" value="2">
								  <label for="star82">2 Star</label>
								  <br>
								  <input type="radio" id="star83" name="overall_rating" required="required" value="3">
								  <label for="star83">3 Star</label>
								   <br>
								  <input type="radio" id="star84" name="overall_rating" required="required" value="4">
								  <label for="star84">4 Star</label>
								   <br>
								  <input type="radio" id="star85" name="overall_rating" required="required" value="5">
								  <label for="star85">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Class of Service (Optional) </label>
							  <p>
								  <input type="radio" id="star9" name="class_rating"  value="1">
								  <label for="star9">1 Star</label>
								  <br>
								  <input type="radio" id="star91" name="class_rating" value="2">
								  <label for="star92">2 Star</label>
								  <br>
								  <input type="radio" id="star93" name="class_rating" value="3">
								  <label for="star93">3 Star</label>
								   <br>
								  <input type="radio" id="star94" name="class_rating" value="4">
								  <label for="star94">4 Star</label>
								   <br>
								  <input type="radio" id="star95" name="class_rating" value="5">
								  <label for="star95">5 Star</label>
								</p>
							</div>
							<div class="col-md-6 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Did You Travel With Children? (Optional)</label>
							  <p>
								  <input type="radio" id="yes" name="children" value="yes">
								  <label for="yes">Yes</label>
								  <br>
								  <input type="radio" id="no" name="children" value="no">
								  <label for="no">No</label>
								</p>
							</div>
							<div class="clearfix"></div>

							<div class="section form-label  form-css-label">
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="reviewsubject" name="reviewsubject" type="text" autocomplete="off"  required/>
							      <label for="reviewsubject">Review Subject (Optional)</label>
							    </fieldset>
							</div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="yourreview" name="yourreview" type="text" autocomplete="off" required   />
							      <label for="yourreview">Your Review (Optional)</label>
							    </fieldset>
							</div>
							<div class="clearfix"></div>
						</div>
							</div>
							<div class="col-md-6">
								<br>
							<button class="proceed main"  type="submit" name="submit">Submit</button></div>
						</div>
				  </form>
				

			</div>
		</div>
	</div>
</section>	
<!-- Search Review Section -->

<!-- Copyright Section -->
<section id="copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<p>Copyright &copy; <?php echo date("Y") ?>  <span>WOW Reviews</span> All Right Reserved.</p>
			</div>
		</div>
	</div>
</section>
</footer>
<!-- End Footer Section -->
</div>
<style type="text/css">
	.Submitreview input{
	width: 100%;

}

.section fieldset {
  margin: 0;
  padding: 0;
  border: 0;
}

.section input {
  display: block;
  width: 100%;
  
  padding: 15px 8px;
  border: 0;
  border-radius: 0;
  font-size: 16px;
  font-weight: 400;
  line-height: 1;
  background: rgba(255, 255, 255, 0.97);
  color: #212121;
  outline: 0;
  -webkit-appearance: none;
     -moz-appearance: none;
          appearance: none;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

.form-label fieldset {
  position: relative;
}

.form-label fieldset + fieldset {
  border-top: 1px solid #ddd;
}
.form-label label {
  position: absolute;
  top: 15px;
  left: 8px;
  color: #909090;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

.form-css-label input[data-empty="false"], .form-css-label input:valid, .form-css-label input:focus {
  padding: 22px 8px 10px;
}
.form-css-label input:focus {
  outline: 0;
  background: white;
}
.form-css-label input[data-empty="false"] + label, .form-css-label input:valid + label, .form-css-label input:focus + label {
  color:#33bae7;
  font-weight: 700;
  font-size: 12px;
  -webkit-transform: translate3d(0, -10px, 0);
          transform: translate3d(0, -10px, 0);
}

.form-js-label input[data-empty="false"], .form-js-label input:focus {
  padding: 22px 8px 10px;
}
.form-js-label input:focus {
  outline: 0;
  background: white;
}
.form-js-label input[data-empty="false"] + label, .form-js-label input:focus + label {
  color: #5856D6;
  font-weight: 700;
  font-size: 12px;
  -webkit-transform: translate3d(0, -10px, 0);
          transform: translate3d(0, -10px, 0);
}


.section h2 {
  margin: 0 0 .5em;
  font-size: 24px;
  font-weight: 400;
  line-height: 1.25;
}
.section p {
  margin: 0 0 1em;
}
.review_ratingcl input{
	width: 15px;
	height: 15px;
}
.review_ratingcl label{
	    vertical-align: 2px !important;
margin-bottom: 10px !important;
}
.labelll{
	font-family: 'poppinsmedium';
	font-size: 16px !important;
	text-transform: capitalize;
	letter-spacing: 1px;
	    color: #000 !important;
}
.main{
	margin:0px 0px !important;
}

</style>
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.js"> </script>

<script type="text/javascript">

	 $( function() {
    $('#journey').datepicker({
     dateFormat: 'd MM yy',
     minDate: 0,
     // onSelect: function(date) {
     //   $("#bookdate").datepicker('option', 'minDate', date);
     // }
   });
   } );

	  $('#result_form').on('submit', function (e) {
            e.preventDefault();
            alert('Hello');
           // $('#getResult').html('Please wait');
            $.ajax({
                type: 'post',
                url: 'ajax.php?action=get_result',
                data: $('form').serialize(),
                success: function (res) {
                   // $('#getResult').html(res);
                    $('#result_form')[0].reset();
                }
                });
            });
</script>