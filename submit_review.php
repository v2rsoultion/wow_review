<?php 
error_reporting(0);
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "wow_temp";

$conn = new mysqli($servername, $username, $password, $dbname);

if(isset($_POST['submit']))
{

 $ins="insert into data_wow set email='".$_POST['email']."',departing='".$_POST['departing']."',arrival='".$_POST['arrival']."',airline='".$_POST['airline']."',flight='".$_POST['flight']."',journey1='".$_POST['journey1']."',seatnumber='".$_POST['seatnumber']."',reviewsubject='".$_POST['reviewsubject']."',yourreview='".$_POST['yourreview']."',punctuality_rating='".$_POST['punctuality_rating']."',service_rating='".$_POST['service_rating']."',comfort_rating='".$_POST['comfort_rating']."',staff_rating='".$_POST['staff_rating']."',beverages_rating='".$_POST['beverages_rating']."',inflight_rating='".$_POST['inflight_rating']."',money_rating='".$_POST['money_rating']."',amenities_rating='".$_POST['amenities_rating']."',overall_rating='".$_POST['overall_rating']."',class_rating='".$_POST['class_rating']."',children='".$_POST['children']."'";


if ($conn->query($ins) === TRUE) {
    $msg= "Thank You for Form submitting";
} else {
    $msg= "Error: " . $ins . "<br>" . $conn->error;
}

	
}

$conn->close();

?>
<!DOCTYPE html>
<html>
   <head>
      <title>WOW Review</title>
      <meta charset="UTF-8">
      <link rel="shortcut icon" type="image/png" href="images/fav.png" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="stylesheet/wow_review.css"/>
      <link rel="stylesheet" type="text/css" href="stylesheet/bootstrap.min.css"/>
      <link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
       <link rel="stylesheet" type="text/css" href="stylesheet/jquery-ui.css">
       <link rel="stylesheet" type="text/css" href="stylesheet/responsive.css">
   </head>
<!-- Header Section -->
   <header>
      <section class="header-top">
         <div class="container-fluid">
         	<!-- Site logo -->
            <div class="col-md-4 logo text-center wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
               <a href="index.php" title="logo">
               <img src="images/logo1.png" alt="logo" >
               </a>
            </div>
          <!-- Site Navigation -->
            <div class="col-md-5 wow_menu text-center">
	               <nav class="navbar navbar-inverse">
		                  <ul class="nav navbar-nav">
		                     <li><a href="" title="Write A Review">Write A Review</a></li>
		                     <li><a href="" title="Search Reviews">Search Reviews</a></li>
		                     <li><a href="" title="Recent Reviews">Recent Reviews</a></li>
		                  </ul>
		            </div>
		            <!-- Login Section -->
		            <div class="col-md-3 padding_remove wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				            <ul class="login_process">
					            <li><a href="" title="login">Log In</a></li> I
					            <li><a href="" title="Sign Up">Sign Up</a></li>
				            </ul>
				            <!-- Social Media Bar -->
				            <div class="social_media">
					            <ul class="">
					            <li><a href="" title="android"><i class="fab fa-android"></i></a></li>
					            I
					            <li><a href="" title="apple"><i class="fab fa-apple"></i></a></li>
				            </ul>
				         </div>
		            </div>
		            <div class="clearfix"></div>
	            </nav>
         </div>
         </div>
      </section>
   </header>
<!-- End Header Section  -->
<!-- Common Section -->
<section id="common_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
				<h1>Submit <span> Your Review</span></h1>
				<!-- <p>Home / <a href="" title="Write a review">Write a review</a></p> -->
			</div>
		</div>
	</div>
</section>

<!-- Search Flight Section -->
<section id="search_flight">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 search_shadow">
				<form class="" action="" method="post" onSubmit="return validateaccsetting()">
				<div class="section form-label  form-css-label">
        <?php if($msg!=""){  ?>
					<div class="alert alert-success">
					  <strong><?php echo @$msg; ?>.</strong>
					</div>
					<?php } ?>
				  			<div class="text-danger"></div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="email" name="email" type="text" autocomplete="off" />
							      <label for="email">Email address*</label>
							    </fieldset>
							</div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="departing" name="departing" type="text" autocomplete="off"  />
							      <label for="departing">Departing Airport*</label>
							    </fieldset>
							</div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="arrival" name="arrival" type="text" autocomplete="off"  />
							      <label for="arrival">Arrival Airport*</label>
							    </fieldset>
							  </div>
							  <div class="clearfix"></div>
							 <div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="airline" name="airline" type="text" autocomplete="off"  />
							      <label for="airline">Airline Name*</label>
							    </fieldset>
							</div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>

							      <input id="flight" name="flight" type="text" autocomplete="off"  />
							      <label for="flight">Flight Number*</label>
							    </fieldset>
							</div>
							<div class="col-md-4 col-xs-12 form-group">
							    <fieldset>
							      <input id="journey" name="journey1" type="text"   />
							      <label for="journey">Date Of Journey*</label>
							    </fieldset>
							</div>


							<div class="clearfix"></div>
							    <div class="col-md-4 col-xs-12 form-group">
							  
							    <fieldset>
							      <input id="seatnumber" name="seatnumber" type="text" autocomplete="off"  />
							      <label for="seatnumber">Seat Number (Optional)</label>
							    </fieldset>
							</div>
							
								<div class="clearfix"></div>
							</div>
							<div class="">
								<br>
								<div class="col-md-12"><b style="font-size: 18px;">Note:- </b><br><span style="font-size: 14px;     font-family: 'poppinsregular'; line-height:30px;     color: #56676b;">1 Star means Poor<br>  2 Star means Average <br>3 Star means Satisfactory<br> 4 Star means Good<br> 5 Star means Excellent</span></div>
								<br>
							<div class="col-md-3 col-xs-12 review_ratingcl">
									
								<br>
							  <label for="rating" class="labelll">Punctuality Rating *</label>
							  <p>
								  <input type="radio" id="s11" name="punctuality_rating" value="1">
								  <label for="p1">1 Star</label>
								  <br>
								  <input type="radio" id="s12" name="punctuality_rating" value="2">
								  <label for="p2">2 Star</label>
								  <br>
								  <input type="radio" id="s13" name="punctuality_rating" value="3">
								  <label for="p3">3 Star</label>
								   <br>
								  <input type="radio" id="s14" name="punctuality_rating" value="4">
								  <label for="p4">4 Star</label>
								   <br>
								  <input type="radio" id="s15" name="punctuality_rating" value="5">
								  <label for="p5">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Ground Service Rating *</label>
							  <p>
								  <input type="radio" id="s21" name="service_rating"    value="1"  >
								  <label for="g1">1 Star</label>
								  <br>
								  <input type="radio" id="s22" name="service_rating"    value="2" >
								  <label for="g2">2 Star</label>
								  <br>
								  <input type="radio" id="s23" name="service_rating"  value="3"  >
								  <label for="g3">3 Star</label>
								   <br>
								  <input type="radio" id="s24" name="service_rating"   value="4" >
								  <label for="g4">4 Star</label>
								   <br>
								  <input type="radio" id="s25" name="service_rating"   value="5" >
								  <label for="g5">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Seat Comfort Rating*</label>
							  <p>
								  <input type="radio" id="s31" name="comfort_rating"    value="1" >
								  <label for="s1">1 Star</label>
								  <br>
								  <input type="radio" id="s32" name="comfort_rating"   value="2" >
								  <label for="s2">2 Star</label>
								  <br>
								  <input type="radio" id="s33" name="comfort_rating"  value="3"  >
								  <label for="s3">3 Star</label>
								   <br>
								  <input type="radio" id="s34" name="comfort_rating"  value="4" >
								  <label for="s4">4 Star</label>
								   <br>
								  <input type="radio" id="s35" name="comfort_rating"  value="5" >
								  <label for="s5">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Cabin Staff Service Rating*</label>
							  <p>
								  <input type="radio" id="s41" name="staff_rating"   value="1" >
								  <label for="c1">1 Star</label>
								  <br>
								  <input type="radio" id="s42" name="staff_rating"   value="2">
								  <label for="c2">2 Star</label>
								  <br>
								  <input type="radio" id="s43" name="staff_rating"   value="3">
								  <label for="c3">3 Star</label>
								   <br>
								  <input type="radio" id="s44" name="staff_rating"   value="4">
								  <label for="c4">4 Star</label>
								   <br>
								  <input type="radio" id="s45" name="staff_rating"   value="5">
								  <label for="c5">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Food & Beverages Rating *</label>
							  <p>
								  <input type="radio" id="s51" name="beverages_rating"   value="1" >
								  <label for="f1">1 Star</label>
								  <br>
								  <input type="radio" id="s52" name="beverages_rating"   value="2">
								  <label for="f2">2 Star</label>
								  <br>
								  <input type="radio" id="s53" name="beverages_rating"  value="3">
								  <label for="f3">3 Star</label>
								   <br>
								  <input type="radio" id="s54" name="beverages_rating"  value="4">
								  <label for="f4">4 Star</label>
								   <br>
								  <input type="radio" id="s55" name="beverages_rating" value="5"  >
								  <label for="f5">5 Star</label>
								</p>
							</div>

							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Inflight Entertainment Rating*</label>
							  <p>
								  <input type="radio" id="s61" name="inflight_rating"  value="1">
								  <label for="i1">1 Star</label>
								  <br>
								  <input type="radio" id="s62" name="inflight_rating" value="2">
								  <label for="i2">2 Star</label>
								  <br>
								  <input type="radio" id="s63" name="inflight_rating" value="3">
								  <label for="i3">3 Star</label>
								   <br>
								  <input type="radio" id="s64" name="inflight_rating" value="4">
								  <label for="i4">4 Star</label>
								   <br>
								  <input type="radio" id="s65" name="inflight_rating" value="5">
								  <label for="i5">5 Star</label>
								</p>
							</div>


							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Value For Money Rating*</label>
							  <p>
								  <input type="radio" id="s71" name="money_rating"  value="1">
								  <label for="v1">1 Star</label>
								  <br>
								  <input type="radio" id="s72" name="money_rating" value="2">
								  <label for="v2">2 Star</label>
								  <br>
								  <input type="radio" id="s73" name="money_rating" value="3">
								  <label for="v3">3 Star</label>
								   <br>
								  <input type="radio" id="s74" name="money_rating" value="4">
								  <label for="v4">4 Star</label>
								   <br>
								  <input type="radio" id="s75" name="money_rating" value="5">
								  <label for="v5">5 Star</label>
								</p>
							</div>

							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Amenities Rating*</label>
							  <p>
								  <input type="radio" id="s81" name="amenities_rating"  value="1" >
								  <label for="a1">1 Star</label>
								  <br>
								  <input type="radio" id="s82" name="amenities_rating" value="2">
								  <label for="a2">2 Star</label>
								  <br>
								  <input type="radio" id="s83" name="amenities_rating" value="3">
								  <label for="a3">3 Star</label>
								   <br>
								  <input type="radio" id="s84" name="amenities_rating" value="4">
								  <label for="a5">4 Star</label>
								   <br>
								  <input type="radio" id="s85" name="amenities_rating" value="5">
								  <label for="a6">5 Star</label>
								</p>
							</div>



                            <div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Cleanliness & Hygiene Rating*</label>
							  <p>
								  <input type="radio" id="s91" name="cleanliness_rating"  value="1" >
								  <label for="h1">1 Star</label>
								  <br>
								  <input type="radio" id="s92" name="cleanliness_rating" value="2">
								  <label for="h2">2 Star</label>
								  <br>
								  <input type="radio" id="s93" name="cleanliness_rating" value="3">
								  <label for="h3">3 Star</label>
								   <br>
								  <input type="radio" id="s94" name="cleanliness_rating" value="4">
								  <label for="h4">4 Star</label>
								   <br>
								  <input type="radio" id="s95" name="cleanliness_rating" value="5">
								  <label for="h6">5 Star</label>
								</p>
						</div>



							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Overall Rating* </label>
							  <p>
								  <input type="radio" id="s101" name="overall_rating"  value="1">
								  <label for="o1">1 Star</label>
								  <br>
								  <input type="radio" id="s102" name="overall_rating" value="2">
								  <label for="o2">2 Star</label>
								  <br>
								  <input type="radio" id="s103" name="overall_rating" value="3">
								  <label for="o3">3 Star</label>
								   <br>
								  <input type="radio" id="s104" name="overall_rating" value="4">
								  <label for="o4">4 Star</label>
								   <br>
								  <input type="radio" id="s105" name="overall_rating" value="5">
								  <label for="o5">5 Star</label>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Class of Service (Optional) </label>
							  <p>
								  <input type="radio" id="" name="class_rating"  value="Business">
								  <label for="m0">Business</label>
								  <br>
								  <input type="radio" id="" name="class_rating" value="Premium Economy">
								  <label for="m1">Premium Economy</label>
								  <br>
								  <input type="radio" id="" name="class_rating" value="Economy">
								  <label for="m2">Economy</label>
								   <br>
								</p>
							</div>
							<div class="col-md-3 col-xs-12 review_ratingcl">
								<br>
							  <label for="rating" class="labelll">Did You Travel With Children? (Optional)</label>
							  <p>
								  <input type="radio" id="yes" name="children" value="yes">
								  <label for="yes">Yes</label>
								  <br>
								  <input type="radio" id="no" name="children" value="no">
								  <label for="no">No</label>
								</p>
							</div>
							<div class="clearfix"></div>

							<div class="section form-label  form-css-label">
							<div class="col-md-6 col-xs-12 form-group moreclassssss">
							    <fieldset>
							   
							      <textarea id="reviewsubject" name="reviewsubject" placeholder="Review Subject (Optional)"></textarea>

							    </fieldset>
							</div>
							<div class="col-md-6 col-xs-12 form-group moreclassssss">
							    <fieldset>

							    	<textarea id="reviewsubject" name="yourreview" placeholder="Your Review (Optional)"></textarea>
							     
							      
							    </fieldset>
							</div>
							<div class="clearfix"></div>
						</div>
							</div>
							<div class="col-md-6">
								<br>
							<button class="proceed main"  type="submit" name="submit">Submit</button></div>
						</div>
				  </form>
				

			</div>
		</div>
	</div>
</section>	
<!-- Search Review Section -->

<!-- Copyright Section -->
<section id="copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<p>Copyright &copy; <?php echo date("Y") ?>  <span>WOW Airlines Reviews</span> All Right Reserved.</p>
			</div>
		</div>
	</div>
</section>
</footer>

</div>
<!-- End Footer Section -->


<style type="text/css">
	.Submitreview input{
	width: 100%;

}

.section fieldset {
  margin: 0;
  padding: 0;
  border: 0;
}

.section input {
  display: block;
  width: 100%;
  
  padding: 15px 8px;
  border: 0;
  border-radius: 0;
  font-size: 16px;
  font-weight: 400;
  line-height: 1;
  background: rgba(255, 255, 255, 0.97);
  color: #212121;
  outline: 0;
  -webkit-appearance: none;
     -moz-appearance: none;
          appearance: none;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

.form-label fieldset {
  position: relative;
}

.form-label fieldset + fieldset {
  border-top: 1px solid #ddd;
}
.form-label label {
  position: absolute;
  top: 15px;
  left: 8px;
  color: #909090;
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

.form-css-label input[data-empty="false"], .form-css-label input:valid, .form-css-label input:focus {
  padding: 22px 8px 10px;
}
.form-css-label input:focus {
  outline: 0;
  background: white;
}
.form-css-label input[data-empty="false"] + label, .form-css-label input:valid + label, .form-css-label input:focus + label {
  color:#33bae7;
  font-weight: 700;
  font-size: 12px;
  -webkit-transform: translate3d(0, -10px, 0);
          transform: translate3d(0, -10px, 0);
}

.form-js-label input[data-empty="false"], .form-js-label input:focus {
  padding: 22px 8px 10px;
}
.form-js-label input:focus {
  outline: 0;
  background: white;
}
.form-js-label input[data-empty="false"] + label, .form-js-label input:focus + label {
  color: #5856D6;
  font-weight: 700;
  font-size: 12px;
  -webkit-transform: translate3d(0, -10px, 0);
          transform: translate3d(0, -10px, 0);
}


.section h2 {
  margin: 0 0 .5em;
  font-size: 24px;
  font-weight: 400;
  line-height: 1.25;
}
.section p {
  margin: 0 0 1em;
}
.review_ratingcl input{
	width: 15px;
	height: 15px;
}
.review_ratingcl label{
	    vertical-align: 2px !important;
margin-bottom: 10px !important;
}
.labelll{
	font-family: 'poppinsmedium';
	font-size: 16px !important;
	text-transform: capitalize;
	letter-spacing: 1px;
	    color: #000 !important;
}
.main{
	margin:0px 0px !important;
}
#reviewsubject{
	resize: none;
	min-height: 100px;
	width: 100%;
	font-family: 'poppinsregular';
    color: #56676b;
    font-size: 14px;
    letter-spacing: 0.5px;
        border-bottom: 2px solid #e2e9f0 !important;
        border: 0px;
}
#reviewsubject:focus{
	 border-bottom: 2px solid #e2e9f0 !important;
        border: 0px;
        outline: none;
}

</style>
<!-- End Footer Section -->
</div>
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.js"> </script>

<script type="text/javascript">
 
	 $( function() {
   
    $('#journey').datepicker({dateFormat: 'd MM yy'});

  } );

function validateaccsetting() {
 	re = /^[A-Za-z ]+$/;
	te = /^[0-9]+$/;
	se = /^[0-9A-Za-z ]+[A-Za-z ]+$/;
	//var i = 1;

 
  var email = document.getElementById('email');
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  
  if (document.getElementById('email').value == ""){
   alert("Please Enter Email Address");
   email.focus();
   return false;
  }
  else
  if (!filter.test(email.value)) {
  alert('Please Enter Correct Email Address');
  document.getElementById('email').focus();
  return false;
  }
  if( document.getElementById('departing').value == "" ) {
   alert('Please Enter Departing Airport');
   document.getElementById('departing').focus();
   return false;
  }

if( document.getElementById('arrival').value == "" ) {
   alert('Please Enter Arrival Airport');
   document.getElementById('arrival').focus();
   return false;
  }
  if( document.getElementById('airline').value == "" ) {
   alert('Please Enter Airline Name');
   document.getElementById('airline').focus();
   return false;
  }
    if( document.getElementById('flight').value == "" ) {
   alert('Please Enter Flight Number');
   document.getElementById('flight').focus();
   return false;
  }
  
 if( document.getElementById('journey').value == "" ) {
   alert('Please Select Date Of Journey');
   document.getElementById('journey').focus();
   return false;
  }

   for (var i = 1; i <= 10; i++) {
  	if (document.getElementById('s'+i+'1').checked == false && document.getElementById('s'+i+'2').checked == false && document.getElementById('s'+i+'3').checked == false && document.getElementById('s'+i+'4').checked == false && document.getElementById('s'+i+'5').checked == false){
	if(i == 1){
		 alert("Please select atleast one Punctuality Rating");
	}
	if(i == 2){
		 alert("Please select atleast one Ground Service Rating");
	}
	if(i == 3){
		 alert("Please select atleast one Seat Comfort Rating");
	}
	if(i == 4){
		 alert("Please select atleast one Cabin Staff Service Rating");
	}
	if(i == 5){
		 alert("Please select atleast one Food & Beverages Rating ");
	}
	if(i == 6){
		 alert("Please select atleast one Inflight Entertainment Rating");
	}
	if(i == 7){
		 alert("Please select atleast one Value For Money Rating");
	}
	if(i == 8){
		 alert("Please select atleast one Amenities Rating");
	}
	if(i == 9){
		 alert("Please select atleast one Cleanliness & Hygiene Rating");
	}
	if(i == 10){
		 alert("Please select atleast one Overall Rating");
	}
	  
	   //email.focus();
	   return false;
	  }

  		
  }

  return true;
}

</script>

