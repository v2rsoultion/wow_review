<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
				<h1>Terms &amp; Conditions</h1>
				<p>Home / Terms &amp; Conditions</p>
			</div>
		</div>
	</div>
</section>

<section id="privacy_policy">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>1. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h3>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				<ul>
					<h3>1.0 Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h3>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
				
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
					<h3>1.1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h3>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
				
				</ul>
			</div>
			<div class="col-md-12">
				<h3>2. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h3>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				<ul>
					<h3>2.0 Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h3>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
				
					<h3>2.1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h3>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
					<li> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
				</ul>
			</div>
		</div>
	</div>
</section>



<?php include("footer.php") ?>

