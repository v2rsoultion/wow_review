<?php include('header.php'); ?>

<!-- Thank You Section -->
<section id="thankyou">
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 text-center wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<div class="thankyou_text">Thank You</div>
				<p>Your voice is heard by millions. Thank you for sharing your feedback.</p>
				<a href="index.php" title="Back To Home">Homepage</a>
			</div>
			<div class="col-md-2"></div>
		</div> 
	</div>
</section>

<?php include('footer.php'); ?>