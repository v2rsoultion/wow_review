<?php include('header.php'); ?>
<!-- Common Section -->
<section id="common_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
				<h1>Write A Review</h1>
				<p>Home / <a href="" title="Write a review">Write a review</a></p>
			</div>
		</div>
	</div>
</section>

<!-- Search Flight Section -->
<section id="search_flight">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 search_shadow wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<form class="form-group" action=""  method="POST" id="search_aero">
					<div class="col-md-1"></div>
					<div class="col-md-5 col-xs-12 form-group" id="selectboxCommon">
						<label>Airline</label>
			      		  <select class="form-control form-group selectpicker" data-show-subtext="true" data-live-search="true" name="aeroname" required="required">
					        <option data-subtext="">Please Select Name </option>
					        <option data-subtext="">Bill Gordon</option>
					        <option data-subtext="">Elizabeth Warren</option>
					        <option data-subtext="">Mario Flores</option>
					        <option data-subtext="">Don Young</option>
     					 </select>
					</div>
					<div class="col-md-5 col-xs-12 form-group" id="selectboxCommon">
						<label>Flight Number</label>
			      		<select class="form-control form-group selectpicker" data-show-subtext="true" data-live-search="true" name="aeroname1" required="required">
					        <option data-subtext="">Please Select Name </option>
					        <option data-subtext="">Bill Gordon</option>
					        <option data-subtext="">Elizabeth Warren</option>
					        <option data-subtext="">Mario Flores</option>
					        <option data-subtext="">Don Young</option>
     					 </select>
					</div>
						<div class="col-md-1"></div>
				<!-- 	<div class="col-md-4 col-xs-12">
						 <label>Date of journey</label>
 						<div class="bfh-datepicker" data-align="right"></div>
					</div> -->
					<div class="clearfix"></div>
					<button type="submit"  name="proceed" class="proceed">Proceed</button>
				</form>
			</div>
		</div>
	</div>
</section>

<!-- Search Review Section -->
<section id="search_review">
	<div class="container">
		<div class="row">
			<div class="col-md-9 remo_padding wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<div class="top_padding">
					<p class="research_today">You're so close. Finish your recent research today!</p>
					<a href="" title="Search Review" class="search_rew_a">Search Review</a>
				<div class="clearfix"></div>
				</div>
				<div class="review_details">
					<div class="review_img">
						<img src="images/r1.png" title="Tigerair">
					</div>
					<div class="review_left">
						<div class="inner_left">
							<h1>8:45 PM -   11:05 PM  </h1>
							<h2>4h 20m Nonstop</h2>
							<h3>Tigerair (TR) · Airbus A319</h3>
						</div>
						<div class="inner_right">
							<span>25 Reviews</span>
							    <i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    			<span class="paddingBottom">On Time Rating <br><small>90%</small></span>
						</div>
						<div class="clearfix"></div>
						<a href="" title="See Reviews"><i class="fas fa-eye"></i> See Reviews</a>
						<a href="" title="Write A Review"><i class="fas fa-edit"></i> Write A Review</a>
						<a href="" title="Share Now"><i class="fas fa-share-alt"></i> Share Now</a>
						<div class="clearfix"></div>

					</div>
					<div class="clearfix"></div>
				</div>
				<div class="review_details">
					<div class="review_img">
						<img src="images/r2.png" title="Amirates">
					</div>
					<div class="review_left">
						<div class="inner_left">
							<h1>8:45 PM -   11:05 PM  </h1>
							<h2>4h 20m Nonstop</h2>
							<h3>Tigerair (TR) · Airbus A319</h3>
						</div>
						<div class="inner_right">
							<span>25 Reviews</span>
							    <i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="fa fa-star" aria-hidden="true"></i>
				    			<i class="far fa-star"></i>
				    			<span class="paddingBottom">On Time Rating <br><small>90%</small></span>
						</div>
						<div class="clearfix"></div>
						<a href="" title="See Reviews"><i class="fas fa-eye"></i> See Reviews</a>
						<a href="" title="Write A Review"><i class="fas fa-edit"></i> Write A Review</a>
						<a href="" title="Share Now"><i class="fas fa-share-alt"></i> Share Now</a>
						<div class="clearfix"></div>

					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-md-3 sidebar_img padding_right1 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<img src="images/left.jpg" alt="Flight">
				<div class="hoverImg">
					<h5>Flights</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>
