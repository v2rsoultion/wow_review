<?php include('header.php'); ?>
<!-- Common Section -->
<?php include('breadcum.php'); ?>
<!-- Write Review Section -->
<section id="review_write">
    <div class="container">
        <div class="row">
            <div class="col-md-9 padding_remove">
                <div class="main_review_write wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <div class="review_write_float">
                        <h3>Emirates · Airbus A380</h3>
                        <p>MEL TO PER</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="note_board">
                	Note : - 
                	<p><span class="star_1">1 Star</span> Poor </p>
                	<p><span class="star_1 star_2">2 Star</span> Average </p>
                	<p><span class="star_1 star_3">3 Star</span> Satisfactory</p>
                	<p><span class="star_1 star_4">4 Star</span> Good</p>
                	<p><span class="star_1 star_5">5 Star</span> Excellent </p>
                </div>

                <!--  here Start Overall Rating Section -->

                <div class="overall_rating">
                	<p>Overall Rating</p>
                <form class="form-group" action="" id="">
	                  <div class="col-md-3 inputBox" id="padd-reviewsss" >
	                      <div class="form-group" id="styles_rating">
	                          <input type="radio" id="rating_one" name="radio-group" required="required" checked="checked">
		                          <label for="rating_one">
		                          	<i class="fas fa-star first_red"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          </label>
	                           <input type="radio" id="rating_two" name="radio-group" required="required">
			                          <label for="rating_two">
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_three" name="radio-group" required="required">
			                          <label for="rating_three">
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_four" name="radio-group" required="required">
			                          <label for="rating_four">
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_five" name="radio-group" required="required">
			                          <label for="rating_five">
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          </label>
	                      </div>
	                  	</div>
	                  	<div class="clearfix"></div>
                 </form>
                </div>
                <div class="rating_section_review">
                <form class="form-group" action="" id="">
	                  <div class="col-md-3 inputBox padding_remove" >
	                  	<p class="overll">Punctuality</p>
	                      <div class="form-group" id="styles_rating">
	                          <input type="radio" id="rating_one1" name="radio-group" required="required" checked="checked">
		                          <label for="rating_one1">
		                          	<i class="fas fa-star first_red"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          </label>
	                           <input type="radio" id="rating_two1" name="radio-group" required="required">
			                          <label for="rating_two1">
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_three1" name="radio-group" required="required">
			                          <label for="rating_three1">
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_four1" name="radio-group" required="required">
			                          <label for="rating_four1">
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_five1" name="radio-group" required="required">
			                          <label for="rating_five1">
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          </label>
	                      </div>
	                  	</div>
	                  	 <div class="col-md-3 inputBox padding_remove" >
	                  	 	<p class="overll">Ground Service</p>
	                      <div class="form-group" id="styles_rating">
	                          <input type="radio" id="rating_one11" name="radio-group" required="required" checked="checked">
		                          <label for="rating_one11">
		                          	<i class="fas fa-star first_red"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          </label>
	                           <input type="radio" id="rating_two11" name="radio-group" required="required">
			                          <label for="rating_two11">
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_three11" name="radio-group" required="required">
			                          <label for="rating_three11">
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_four11" name="radio-group" required="required">
			                          <label for="rating_four11">
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_five11" name="radio-group" required="required">
			                          <label for="rating_five11">
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          </label>
	                      </div>
	                  	</div>
	                  	 <div class="col-md-3 inputBox padding_remove" >
	                  	 	<p class="overll">Seat Comfort </p>
	                      <div class="form-group" id="styles_rating">
	                          <input type="radio" id="rating_one111" name="radio-group" required="required" checked="checked">
		                          <label for="rating_one111">
		                          	<i class="fas fa-star first_red"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          </label>
	                           <input type="radio" id="rating_two111" name="radio-group" required="required">
			                          <label for="rating_two111">
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_three111" name="radio-group" required="required">
			                          <label for="rating_three111">
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_four111" name="radio-group" required="required">
			                          <label for="rating_four111">
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_five111" name="radio-group" required="required">
			                          <label for="rating_five111">
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          </label>
	                      </div>
	                  	</div>
	                  	 <div class="col-md-3 inputBox padding_remove" >
	                  	 	<p class="overll">Cabin Staff Service</p>
	                      <div class="form-group" id="styles_rating">
	                          <input type="radio" id="rating_one122" name="radio-group" required="required" checked="checked">
		                          <label for="rating_one122">
		                          	<i class="fas fa-star first_red"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          </label>
	                           <input type="radio" id="rating_two122" name="radio-group" required="required">
			                          <label for="rating_two122">
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_three122" name="radio-group" required="required">
			                          <label for="rating_three122">
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_four122" name="radio-group" required="required">
			                          <label for="rating_four122">
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_five122" name="radio-group" required="required">
			                          <label for="rating_five122">
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          </label>
	                      </div>
	                  	</div>
	                  	 <div class="col-md-3 inputBox padding_remove" >
	                  	 	<p class="overll">WiFi & Connectivity</p>
	                      <div class="form-group" id="styles_rating">
	                          <input type="radio" id="rating_one12235" name="radio-group" required="required" checked="checked">
		                          <label for="rating_one12235">
		                          	<i class="fas fa-star first_red"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          </label>
	                           <input type="radio" id="rating_two12235" name="radio-group" required="required">
			                          <label for="rating_two12235">
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_three12235" name="radio-group" required="required">
			                          <label for="rating_three12235">
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_four12235" name="radio-group" required="required">
			                          <label for="rating_four12235">
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_five12235" name="radio-group" required="required">
			                          <label for="rating_five12235">
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          </label>
	                      </div>
	                  	</div>
	                  	 <div class="col-md-3 inputBox padding_remove" >
	                  	 	<p class="overll">Children friendliness</p>
	                      <div class="form-group" id="styles_rating">
	                          <input type="radio" id="rating_one12231" name="radio-group" required="required" checked="checked">
		                          <label for="rating_one12231">
		                          	<i class="fas fa-star first_red"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          </label>
	                           <input type="radio" id="rating_two12231" name="radio-group" required="required">
			                          <label for="rating_two12231">
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_three12231" name="radio-group" required="required">
			                          <label for="rating_three12231">
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_four12231" name="radio-group" required="required">
			                          <label for="rating_four12231">
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_five12231" name="radio-group" required="required">
			                          <label for="rating_five12231">
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          </label>
	                      </div>
	                  	</div>
	                  	<div class="col-md-3 inputBox padding_remove" >
	                  	 	<p class="overll">Value for money</p>
	                      <div class="form-group" id="styles_rating">
	                          <input type="radio" id="rating_one122311" name="radio-group" required="required" checked="checked">
		                          <label for="rating_one122311">
		                          	<i class="fas fa-star first_red"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          </label>
	                           <input type="radio" id="rating_two122311" name="radio-group" required="required">
			                          <label for="rating_two122311">
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_three122311" name="radio-group" required="required">
			                          <label for="rating_three122311">
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_four122311" name="radio-group" required="required">
			                          <label for="rating_four122311">
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_five122311" name="radio-group" required="required">
			                          <label for="rating_five122311">
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          </label>
	                      </div>
	                  	</div>
	                  	<div class="col-md-3 inputBox padding_remove" >
	                  	 	<p class="overll">Amenities</p>
	                      <div class="form-group" id="styles_rating">
	                          <input type="radio" id="rating_one1223112" name="radio-group" required="required" checked="checked">
		                          <label for="rating_one1223112">
		                          	<i class="fas fa-star first_red"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          </label>
	                           <input type="radio" id="rating_two1223112" name="radio-group" required="required">
			                          <label for="rating_two1223112">
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_three1223112" name="radio-group" required="required">
			                          <label for="rating_three1223112">
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_four1223112" name="radio-group" required="required">
			                          <label for="rating_four1223112">
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_five1223112" name="radio-group" required="required">
			                          <label for="rating_five1223112">
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          </label>
	                      </div>
	                  	</div>
	                  	<div class="col-md-3 inputBox padding_remove" >
	                  	 	<p class="overll">Cleanliness & hygiene</p>
	                      <div class="form-group" id="styles_rating">
	                          <input type="radio" id="rating_one12231124" name="radio-group" required="required" checked="checked">
		                          <label for="rating_one12231124">
		                          	<i class="fas fa-star first_red"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          	<i class="fas fa-star"></i>
		                          </label>
	                           <input type="radio" id="rating_two12231124" name="radio-group" required="required">
			                          <label for="rating_two12231124">
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star two_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_three12231124" name="radio-group" required="required">
			                          <label for="rating_three12231124">
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star three_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_four12231124" name="radio-group" required="required">
			                          <label for="rating_four12231124">
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star four_rating"></i>
			                          	<i class="fas fa-star"></i>
			                          </label>
	                           <input type="radio" id="rating_five12231124" name="radio-group" required="required">
			                          <label for="rating_five12231124">
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          	<i class="fas fa-star rating_five"></i>
			                          </label>
	                      </div>
	                  	</div>
	                  		<div class="clearfix"></div>
	                  	  <div class="col-md-3 col-xs-6 padd_left">
                             <button type="submit" name="submit" class="rating_submit">SUBMIT</button>
												</div>
												 <div class="col-md-9 col-xs-6">
												 <a class="collapseExample1" title="Expand More Fields" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="hide_review()">Expand More Fields <i class="fas fa-chevron-down"></i></a>
												</div>
	                  	<div class="clearfix"></div>
                 </form>

                </div>
                <div class="thanks_setion">
                    <div class="sigle_line">
                        <!-- <p><img src="images/ptag.png" abs="middle" alt="i_tag"> No rating indicates not applicable to the flight.</p> -->
                    </div>
                    <!-- Close Service Section -->
                    <div class="service_form">
                       
                       
                        <div class="clearfix"></div>

                        <div class="collapse" id="collapseExample">
                        	 <h4>Class of Service</h4>
                            <div class="card card-body">
                                <form class="form-group" action="" id="writereview">
                                    <div class="col-md-12 padd_left Common_form inputBox radion1">
                                        <div class="form-group">
                                            <input type="radio" id="Economy" name="radio-group" required="required">
                                            <label for="Economy">Economy</label>

                                            <input type="radio" id="Premium" name="radio-group" required="required">
                                            <label for="Premium">Premium Economy</label>

                                            <input type="radio" id="Business" name="radio-group" required="required">
                                            <label for="Business">Business</label>

                                            <input type="radio" id="Class" name="radio-group" required="required">
                                            <label for="Class">First Class</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padd_left Common_form">
                                        <label>Date of journey</label>
                                        <div class="bfh-datepicker extra_form" data-align="right"></div>
                                    </div>
                                    <div class="col-md-6 padd_left Common_form inputBox">
                                        <label>Seat Number</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="seat" placeholder="Enter Seat Number" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-12 padd_left Common_form inputBox">
                                        <label>Subject</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="subject" placeholder="Subject" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-12 padd_left Common_form inputBox">
                                        <div class="form-group">
                                            <label>Your Review</label>
                                            <textarea class="form-control" required="required" name="writereview" placeholder="Tell people about your experience with Emirates"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 padd_left Common_form inputBox compaDD">
                                        <label>What sort of visit was this?</label>
                                        <div class="form-group">
                                            <select class="form-control" name="business" required="required">
																			   				<option value="">Select Something</option>
																			   				<option value="Business">Business</option>
																			   				<option value="Business1">Business</option>
				   																	</select>
                                        </div>
                                    </div>
                                    <div class="col-md-5 padd_left Common_form inputBox">
                                        <label>What sort of visit was this? 
				   													</div>
														   			<div class="col-md-7 padd_left Common_form inputBox radion">
																	    <input type="radio" id="yes" name="radio-group" checked>
																	   		<label for="yes">Yes</label>
                                        <input type="radio" id="no" name="radio-group">
                                        <label for="no">No</label>
                                    </div>
                                    <div class="col-md-12 padd_left fileupload">
                                        <p>Do you have photo to share?</p>
                                        <div class="mainFileUpload">
                                            Add Photo
                                            <input id="fileupload" type="file" multiple="multiple" class="hide_file" />
                                        </div>
                                       	 <div class="clearfix"></div>
                                       	 <div id="dvPreview"></div>
                                    </div>
                                    <div class="col-md-12 padd_left Common_form inputBox">
                                        <label>Any tips for fellow travellers</label>
                                        	<div class="form-group">
                                            <input type="text" class="form-control" name="travellers" placeholder="Enter Any tips for fellow travellers">
                                        	</div>
                                    </div>
                                    <div class="col-md-3 padd_left">
                                        <button type="submit" name="submit">SUBMIT</button>
                                    </div>
                                </form>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-3 common_side padding_right1">
                <div class="container1">
                    <img src="images/side2.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>
                <div class="container1">
                    <img src="images/side1.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>
                <div class="container1">
                    <img src="images/side2.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>
                <div class="container1">
                    <img src="images/side5.jpg" alt="Avatar" class="image">
                    <div class="overlay">
                        <div class="text">
                            <h2>Emirates</h2>
                            <span>Airbus A380</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!-- Include Footer  -->
<?php include('footer.php'); ?>